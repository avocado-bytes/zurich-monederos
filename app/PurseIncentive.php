<?php

namespace ZurichMonederos;

use Illuminate\Database\Eloquent\Model;

class PurseIncentive extends Model
{

	const REGIONES = [
        'Corporativo Call Center' => 'Corporativo Call Center', 
        'Corporativo Asesores' => 'Corporativo Asesores',
        'Corporativo'=>'Corporativo',
        'Metro Sur'=>'Metro Sur',
        'Metro Norte'=>'Metro Norte',
        'Norte'=>'Norte','Noreste'=>'Noreste',
        'Noroeste'=>'Noroeste','Centro'=>'Centro',
        'Sur'=>'Sur','Sureste'=>'Sureste',
        'Occidente'=>'Occidente',
        'Sin Región'=>'Sin Región'
    ];
    protected $fillable = [
		"nombre_trabajador",
		"id_tgs_si_vale",
		"identificador",
		"no_tarjeta",
		"concepto",
		"region",
		"estatus",
		"fecha_de_envio",
		"monto_de_fondeo",
		"fecha_de_fondeo",
		"empleado",
		"mes",
		"anio",
	];
	protected $hidden = [
		'id',
		'created_at',
		'updated_at'
	];
}
