<?php

namespace ZurichMonederos;

use Illuminate\Database\Eloquent\Model;

class CardRequest extends Model
{
     protected $fillable = [
		"cantidad",
		"numero_pedido",
		"fecha_solicitud",
		"purse",
	];
}
