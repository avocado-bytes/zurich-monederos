<?php

namespace ZurichMonederos\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GasChargesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'no_tarjeta' => 'min:16|max:16',
        'id_tgs_si_vale' => 'required',
        'identificador' => 'required',
        ];
    }
}
