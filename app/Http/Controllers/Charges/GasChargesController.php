<?php

namespace ZurichMonederos\Http\Controllers\Charges;


use Illuminate\Http\Request;
use ZurichMonederos\Http\Requests\GasChargesRequest;
use ZurichMonederos\Http\Controllers\Controller;
use ZurichMonederos\PurseGas;
use PHPExcel; 
use PHPExcel_IOFactory;
use Session;
use File;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;



class GasChargesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $purses = PurseGas::all();
        if ($last = $purses->last()==null) {
            $last = 1;
            return view('charges.gas.index', compact('purses','last'));
        }
        $last = $purses->last()->id;
        $last = $last+1;
        return view('charges.gas.index', compact('purses','last'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GasChargesRequest $request)
    {
        $purses= PurseGas::all();
        $purseGas = new PurseGas($request->all());
        $purseGas->save();
        return redirect()->action('Charges\GasChargesController@index')->withSuccess('Se ha guardado el registro.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {

    }



    public function bindValue(PHPExcel_Cell $cell, $value = null)
    {
        if (is_numeric($value))
        {
            $cell->setValueExplicit($value, PHPExcel_Cell_DataType::TYPE_STRING);
            return true;
        }
        // else return default behavior
        return parent::bindValue($cell, $value);
    }


    /**
     * Import file into database Code
     *
     * @var array
     */
    public function ImportPurses (){

        $file = Input::file('file');
        try {
            $file_name = $file->getClientOriginalName();
        } 
        catch (\Throwable $e) {
            return redirect()->action('Charges\GasChargesController@index')->withWarning('Por favor seleccione una archivo .xls.');
        } 
        $file->move('files',$file_name);
        $results = Excel::load('files/'.$file_name, function($reader) {
        })->get(['nombre_trabajador',
        'id_tgs_si_vale',
        'identificador',
        'no_tarjeta',
        'concepto',
        'estatus',
        'fecha_de_envio',
        'monto_de_fondeo',
        'empleado']);
        foreach ($results as $key => $value) {
           try {
            $insert[] = ['nombre_trabajador' => $value->nombre_trabajador,
                           'id_tgs_si_vale' => $value->id_tgs_si_vale,
                           'identificador' => $value->identificador,
                           'no_tarjeta' => strval($value->no_tarjeta),
                           'concepto' => $value->concepto,
                           'estatus' => $value->estatus,
                           'fecha_de_envio' => $value->fecha_de_envio,
                           'monto_de_fondeo' => $value->monto_de_fondeo,
                           'empleado' => $value->empleado
                         ];
         } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->action('Charges\GasChargesController@index')->withDanger('¡Error! Hubo un problema en la carga del archivo.');
        } 
    }
    PurseGas::insert($insert);
    return redirect()->action('Charges\GasChargesController@index')->withSuccess('Se ha guardado el registro.');
}



             // foreach ($results->toArray() as $key => $value) {  
             //        if(!empty($value)){
             //            foreach ($results as $key => $value) {
             //                $insert[] = ['nombre_trabajador' => $value->nombre_trabajador, 'id_tgs_si_vale' => $value->id_tgs_si_vale, 'identificador' => $value->identificador, 'no_tarjeta' => $value->no_tarjeta, 'concepto' => $value->concepto, 'estatus' => $value->estatus, 'fecha_de_envio' => $value->fecha_de_envio, 'monto_de_fondeo' => $value->monto_de_fondeo, 'empleado' => $value->empleado];
             //            }
             //        }
             //    }
             //    if(!empty($insert)){
             //        PurseGas::insert($insert);
             //        return redirect()->action('Charges\GasChargesController@index')->withSuccess('Insert Record successfully.');
             //    }




//     public function import(Request $request){
//     //validate the xls file
//     $this->validate($request, array(
//         'file'      => 'required'
//     ));

//     if($request->hasFile('file')){
//         $extension = File::extension($request->file->getClientOriginalName());
//         if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

//             $path = $request->file->getRealPath();
//             $data = Excel::load($path, function($reader) {
//             })->get();
//             if(!empty($data) && $data->count()){

//                 foreach ($data->toArray() as $key => $value) {
//                     if(!empty($value)){
//                         foreach ($data as $key => $value) {
//                             $insert[] = ['nombre_trabajador' => $value->nombre_trabajador, 'id_tgs_si_vale' => $value->id_tgs_si_vale, 'identificador' => $value->identificador, 'no_tarjeta' => $value->no_tarjeta, 'concepto' => $value->concepto, 'estatus' => $value->estatus, 'fecha_de_envio' => $value->fecha_de_envio, 'monto_de_fondeo' => $value->monto_de_fondeo, 'empleado' => $value->empleado];
//                         }
//                     }
//                 }

//                 if(!empty($insert)){
//                    $insertData = DB::table('purse_gas')->insert($insert);     
//                     if ($insertData) {
//                         return redirect()->action('Charges\GasChargesController@index')->withSuccess('Your Data has successfully imported');
//                     }else {                        
//                         // Session::flash('error', 'Error inserting the data..');
//                         return redirect()->action('Charges\GasChargesController@index')->withSuccess('Error inserting the data..');
//                     }
//                 }
//             }

//             return back();

//         }else {
//             // Session::flash('error', 'File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!');
//             return redirect()->action('Charges\GasChargesController@index')->withSuccess('file.!! Please upload a valid xls/csv file..!!');
//         }
//     }
// }


    // funcion importar excel 2 try

    // public function importExcel(Request $request)
    // {
    //     if($request->hasFile('import_file')){
    //         $path = $request->file('import_file')->getRealPath();
    //         $data = Excel::load($path, function($reader) {})->get();
    //         if(!empty($data) && $data->count()){
    //             foreach ($data->toArray() as $key => $value) {
    //                 if(!empty($value)){
    //                     foreach ($data as $key => $value) {
    //                         $insert[] = ['nombre_trabajador' => $value->nombre_trabajador, 'id_tgs_si_vale' => $value->id_tgs_si_vale, 'identificador' => $value->identificador, 'no_tarjeta' => $value->no_tarjeta, 'concepto' => $value->concepto, 'estatus' => $value->estatus, 'fecha_de_envio' => $value->fecha_de_envio, 'monto_de_fondeo' => $value->monto_de_fondeo, 'empleado' => $value->empleado];
    //                     }
    //                 }
    //             }
    //             if(!empty($insert)){
    //                 PurseGas::insert($insert);
    //                 return redirect()->action('Charges\GasChargesController@index')->withSuccess('Insert Record successfully.');
    //             }
    //         }
    //     }
    //     return redirect()->action('Charges\GasChargesController@index')->withError('Please Check your file, Something is wrong there.');
    // }
}
