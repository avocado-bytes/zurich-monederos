<?php

namespace ZurichMonederos\Http\Controllers\Charges;

use Illuminate\Http\Request;
use ZurichMonederos\Http\Controllers\Controller;
use ZurichMonederos\Http\Requests\GasChargesRequest;
use ZurichMonederos\PurseIncentive;
use PHPExcel; 
use PHPExcel_IOFactory;
use Session;
use File;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use Symfony\Component\Debug\Exception\FatalThrowableError;

class IncentivosChargesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $purses = PurseIncentive::all();
      if ($last = $purses->last()==null) {
        $last = 1;
        return view('charges.incentivos.index', compact('purses','last'));
    }
    $last = $purses->last()->id;
    $last = $last+1;
    return view('charges.incentivos.index', compact('purses','last'));

}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GasChargesRequest $request)
    {
      $purseIncentive = new PurseIncentive($request->all());
      $purseIncentive->save();
      return redirect()->action('Charges\IncentivosChargesController@index')->withSuccess('Se ha guardado el registro.');
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {

    }
        /**
     * Import file into database Code          
     *
     * @var array
     */
        public function ImportPurses (){

            $file = Input::file('file');
            try {
                $file_name = $file->getClientOriginalName();
            } catch (\Throwable $e) {
                return redirect()->action('Charges\IncentivosChargesController@index')->withWarning('Por favor seleccione una archivo .xls.');
            } 
            $file->move('files',$file_name);
            $results = Excel::load('files/'.$file_name, function($reader) {
                $reader->all();     
            })->get(['nombre_trabajador',
            'id_tgs_si_vale',
            'identificador',
            'no_tarjeta',
            'concepto',
            'region',
            'estatus',
            'fecha_de_envio',
            'monto_de_fondeo',
            'fecha_de_fondeo',
            'empleado',
            'mes',
            'anio']);
            foreach ($results as $key => $value) {
                try {
                   $insert[] = ['nombre_trabajador' => $value->nombre_trabajador,
                   'id_tgs_si_vale' => $value->id_tgs_si_vale,
                   'identificador' => $value->identificador,
                   'no_tarjeta' => strval($value->no_tarjeta),
                   'concepto' => $value->concepto,
                   'region' => $value->region,
                   'estatus' => $value->estatus,
                   'fecha_de_envio' => $value->fecha_de_envio,
                   'monto_de_fondeo' => $value->monto_de_fondeo,
                   'fecha_de_fondeo' => $value->fecha_de_fondeo,
                   'empleado' => $value->empleado,
                   'mes' => $value->mes,
                   'anio' => $value->anio
                   ];
               } catch (\Illuminate\Database\QueryException $e) {
                 return redirect()->action('Charges\IncentivosChargesController@index')->withDanger('¡Error! Hubo un problema en la carga del archivo.');
             }
         }
              PurseIncentive::insert($insert);
         return redirect()->action('Charges\IncentivosChargesController@index')->withSuccess('Se ha guardado el registro.');

     }

 }























