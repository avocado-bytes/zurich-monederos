<?php

namespace ZurichMonederos\Http\Controllers\Charges;

use Illuminate\Http\Request;
use ZurichMonederos\Http\Requests\GasChargesRequest;
use ZurichMonederos\Http\Controllers\Controller;
use ZurichMonederos\PurseTravelExpense;
use PHPExcel; 
use PHPExcel_IOFactory;
use Session;
use File;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;

class ViaticosChargesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $purses = PurseTravelExpense::all();
        if ($last = $purses->last()==null) {
            $last = 1;
            return view('charges.viaticos.index', compact('purses','last'));
        }
        $last = $purses->last()->id;
        $last = $last+1;
        return view('charges.viaticos.index', compact('purses','last'));

   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GasChargesRequest $request)
    {
        $PurseTravelExpense = new PurseTravelExpense($request->all());
        $PurseTravelExpense->save();
        return redirect()->action('Charges\ViaticosChargesController@index')->withSuccess('Se ha guardado el registro.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {

    }
    /**
     * Import file into database Code
     *
     * @var array
     */
    public function ImportPurses (){

        $file = Input::file('file');
        try {
            $file_name = $file->getClientOriginalName();
        } catch (\Throwable $e) {
            return redirect()->action('Charges\ViaticosChargesController@index')->withWarning('Por favor seleccione una archivo .xls.');
        } 
        $file->move('files',$file_name);
        $results = Excel::load('files/'.$file_name, function($reader) {
            $reader->all();   
        })->get(['nombre_trabajador',
        'id_tgs_si_vale',
        'identificador',
        'no_tarjeta',
        'concepto',
        'estatus',
        'fecha_de_envio',
        'acumulado',
        'segmento',
        'empleado',
        'folio_reasignacion']);
        foreach ($results as $key => $value) {
            try {
                $insert[] = ['nombre_trabajador' => $value->nombre_trabajador,
                   'id_tgs_si_vale' => $value->id_tgs_si_vale,
                   'identificador' => $value->identificador,
                   'no_tarjeta' => strval($value->no_tarjeta),
                   'concepto' => $value->concepto,
                   'estatus' => $value->estatus,
                   'fecha_de_envio' => $value->fecha_de_envio,
                   'acumulado' => $value->acumulado,
                   'segmento' => $value->segmento,
                   'empleado' => $value->empleado,
                   'folio_reasignacion' => $value->folio_reasignacion
                   ];
         } catch (\Illuminate\Database\QueryException $e) {
             return redirect()->action('Charges\ViaticosChargesController@index')->withDanger('¡Error! Hubo un problema en la carga del archivo.');
         }
    }
PurseTravelExpense::insert($insert);
    return redirect()->action('Charges\ViaticosChargesController@index')->withSuccess('Se ha guardado el registro.');

}

}
