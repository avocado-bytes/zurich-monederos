<?php

namespace ZurichMonederos\Http\Controllers\Summaries;

use Illuminate\Http\Request;
use ZurichMonederos\Http\Controllers\Controller;
use ZurichMonederos\PurseIncentive;
use Illuminate\Support\Facades\DB;
use ZurichMonederos\TotalFacture;

class IncentivosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $disponibleInicial = 190814.08; 
       $abonoEnero = DB::table('total_factures')->where('purse','incentives')->where('mes','Enero')->whereRaw('MONTH(fecha_fondeo)= ?',[01])->get()->sum('monto_fondeo');
       $abonoFebrero = DB::table('total_factures')->where('purse','incentives')->where('mes','Febrero')->whereRaw('MONTH(fecha_fondeo)= ?',[02])->get()->sum('monto_fondeo');
       $abonoMarzo = DB::table('total_factures')->where('purse','incentives')->where('mes','Marzo')->whereRaw('MONTH(fecha_fondeo)= ?',[03])->get()->sum('monto_fondeo');
       $abonoAbril = DB::table('total_factures')->where('purse','incentives')->where('mes','Abril')->whereRaw('MONTH(fecha_fondeo)= ?',[04])->get()->sum('monto_fondeo');
       $abonoMayo = DB::table('total_factures')->where('purse','incentives')->where('mes','Mayo')->whereRaw('MONTH(fecha_fondeo)= ?',[05])->get()->sum('monto_fondeo');
       $abonoJunio = DB::table('total_factures')->where('purse','incentives')->where('mes','Junio')->whereRaw('MONTH(fecha_fondeo)= ?',[06])->get()->sum('monto_fondeo');
       $abonoJulio = DB::table('total_factures')->where('purse','incentives')->where('mes','Julio')->whereRaw('MONTH(fecha_fondeo)= ?',[07])->get()->sum('monto_fondeo');
       $abonoAgosto = DB::table('total_factures')->where('purse','incentives')->where('mes','Agosto')->whereRaw('MONTH(fecha_fondeo)= ?',[8])->get()->sum('monto_fondeo');
       $abonoSeptiembre = DB::table('total_factures')->where('purse','incentives')->where('mes','Septiembre')->whereRaw('MONTH(fecha_fondeo)= ?',[9])->get()->sum('monto_fondeo');
       $abonoOctubre = DB::table('total_factures')->where('purse','incentives')->where('mes','Octubre')->whereRaw('MONTH(fecha_fondeo)= ?',[10])->get()->sum('monto_fondeo');
       $abonoNoviembre = DB::table('total_factures')->where('purse','incentives')->where('mes','Noviembre')->whereRaw('MONTH(fecha_fondeo)= ?',[11])->get()->sum('monto_fondeo');
       $abonoDiciembre = DB::table('total_factures')->where('purse','incentives')->where('mes','Diciembre')->whereRaw('MONTH(fecha_fondeo)= ?',[12])->get()->sum('monto_fondeo');
       $totalabono = $abonoEnero+$abonoFebrero+$abonoMarzo+$abonoAbril+$abonoMayo +$abonoJunio+$abonoJulio+$abonoAgosto+$abonoSeptiembre+$abonoOctubre+$abonoNoviembre+$abonoDiciembre;


       $dispersionEnero = DB::table('purse_incentives')->whereRaw('MONTH(fecha_de_envio)= ?',[01])->get()->sum('monto_de_fondeo');
       $dispersionFebrero = DB::table('purse_incentives')->whereRaw('MONTH(fecha_de_envio)= ?',[02])->get()->sum('monto_de_fondeo');
       $dispersionMarzo = DB::table('purse_incentives')->whereRaw('MONTH(fecha_de_envio)= ?',[03])->get()->sum('monto_de_fondeo');
       $dispersionAbril = DB::table('purse_incentives')->whereRaw('MONTH(fecha_de_envio)= ?',[04])->get()->sum('monto_de_fondeo');
       $dispersionMayo = DB::table('purse_incentives')->whereRaw('MONTH(fecha_de_envio)= ?',[05])->get()->sum('monto_de_fondeo');
       $dispersionJunio = DB::table('purse_incentives')->whereRaw('MONTH(fecha_de_envio)= ?',[06])->get()->sum('monto_de_fondeo');
       $dispersionJulio = DB::table('purse_incentives')->whereRaw('MONTH(fecha_de_envio)= ?',[07])->get()->sum('monto_de_fondeo');
       $dispersionAgosto = DB::table('purse_incentives')->whereRaw('MONTH(fecha_de_envio)= ?',[8])->get()->sum('monto_de_fondeo');
       $dispersionSeptiembre = DB::table('purse_incentives')->whereRaw('MONTH(fecha_de_envio)= ?',[9])->get()->sum('monto_de_fondeo');
       $dispersionOctubre = DB::table('purse_incentives')->whereRaw('MONTH(fecha_de_envio)= ?',[10])->get()->sum('monto_de_fondeo');
       $dispersionNoviembre = DB::table('purse_incentives')->whereRaw('MONTH(fecha_de_envio)= ?',[11])->get()->sum('monto_de_fondeo');
       $dispersionDiciembre = DB::table('purse_incentives')->whereRaw('MONTH(fecha_de_envio)= ?',[12])->get()->sum('monto_de_fondeo');
       $totaldispersion = $dispersionEnero+$dispersionFebrero+$dispersionMarzo+$dispersionAbril+$dispersionMayo +$dispersionJunio+$dispersionJulio+$dispersionAgosto+$dispersionSeptiembre+$dispersionOctubre+$dispersionNoviembre+$dispersionDiciembre;
       
       $disponibleFinalEnero = $disponibleInicial + $abonoEnero - $dispersionEnero;
       $disponibleFinalFebrero = $disponibleInicial +$abonoFebrero - $dispersionFebrero;
       $disponibleFinalMarzo = $disponibleInicial +  $abonoMarzo - $dispersionMarzo;
       $disponibleFinalAbril = $disponibleInicial + $abonoAbril - $dispersionAbril ;
       $disponibleFinalMayo = $disponibleInicial + $abonoMayo - $dispersionMayo;
       $disponibleFinalJunio = $disponibleInicial + $abonoJunio - $dispersionJunio ;
       $disponibleFinalJulio = $disponibleInicial +  $abonoJulio - $dispersionJulio ;
       $disponibleFinalAgosto =$disponibleInicial + $abonoAgosto  - $dispersionAgosto;
       $disponibleFinalSeptiembre = $disponibleInicial +  $abonoSeptiembre - $dispersionSeptiembre;
       $disponibleFinalOctubre = $disponibleInicial + $abonoOctubre - $dispersionOctubre;
       $disponibleFinalNoviembre = $disponibleInicial + $abonoNoviembre - $dispersionNoviembre;
       $disponibleFinalDiciembre = $disponibleInicial + $abonoDiciembre - $dispersionDiciembre;

      return view('summaries.incentivos.index',compact('dispersionEnero','dispersionFebrero','dispersionMarzo','dispersionAbril','dispersionMayo','dispersionJunio','dispersionJulio','dispersionAgosto','dispersionSeptiembre','dispersionOctubre','dispersionNoviembre','dispersionDiciembre','totaldispersion','abonoEnero','abonoFebrero','abonoMarzo','abonoAbril','abonoMayo','abonoJunio','abonoJulio','abonoAgosto','abonoSeptiembre','abonoOctubre','abonoNoviembre','abonoDiciembre','totalabono','disponibleInicial','disponibleFinalEnero','disponibleFinalFebrero','disponibleFinalMarzo','disponibleFinalAbril','disponibleFinalMayo','disponibleFinalJunio','disponibleFinalJulio','disponibleFinalAgosto','disponibleFinalSeptiembre','disponibleFinalOctubre','disponibleFinalNoviembre','disponibleFinalDiciembre'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        
    }
}
