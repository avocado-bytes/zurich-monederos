<?php

namespace ZurichMonederos\Http\Controllers;

use Illuminate\Http\Request;
use ZurichMonederos\Http\Controllers\Controller;
use ZurichMonederos\CardRequest;

class CardRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cardrequestsGas = CardRequest::all()->where('purse', 'gas');
        $cardrequestsIncentivos = CardRequest::all()->where('purse', 'incentives');
        $cardrequestsViaticos = CardRequest::all()->where('purse', 'travel_expensive');

        return view('cardRequests.gas.index', compact('cardrequestsGas','cardrequestsIncentivos','cardrequestsViaticos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cardrequest = new CardRequest($request->all());
        if ($cardrequest->purse == 'Gasolina') {
            $cardrequest->purse = 'gas';
            $cardrequest->save();
            return redirect()->action('CardRequestController@index')->withSuccess('Se ha guardado el registro.');
        }elseif ($cardrequest->purse == 'Incentivos') {
            $cardrequest->purse = 'incentives';
            $cardrequest->save();
            return redirect()->action('CardRequestController@index')->withSuccess('Se ha guardado el registro.');
        }elseif ($cardrequest->purse == 'Viaticos') {
            $cardrequest->purse = 'travel_expensive';
            $cardrequest->save();
         return redirect()->action('CardRequestController@index')->withSuccess('Se ha guardado el registro.');
     }
     return redirect()->action('CardRequestController@index')->withDanger('Hubo un error');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        
    }
}
