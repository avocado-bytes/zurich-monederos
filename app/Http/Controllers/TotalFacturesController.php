<?php

namespace ZurichMonederos\Http\Controllers;

use Illuminate\Http\Request;

use ZurichMonederos\Http\Controllers\Controller;
use ZurichMonederos\TotalFacture;
use ZurichMonederos\PurseGas;
use ZurichMonederos\PurseIncentive;
use ZurichMonederos\PurseTravelExpense;
class TotalFacturesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $facturesGas = TotalFacture::all()->where('purse', 'gas');
        $facturesIncentivos = TotalFacture::all()->where('purse', 'incentives');
        $facturesViaticos = TotalFacture::all()->where('purse', 'travel_expensive');
        
        $totalMontoGas = PurseGas::sum('monto_de_fondeo');
        $totalMontoIncentivos = PurseIncentive::sum('monto_de_fondeo');
        $totalMontoViaticos = PurseTravelExpense::sum('acumulado');

        return view('totalFactures.gas.index', compact('facturesGas','facturesIncentivos','facturesViaticos','totalMontoGas','totalMontoIncentivos','totalMontoViaticos'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $facture = new TotalFacture($request->all());
        if ($facture->purse == 'Gasolina') {
            $facture->purse = 'gas';
            $facture->total = $request->comision + $request->monto_fondeo;
            $facture->save();
            return redirect()->action('TotalFacturesController@index')->withSuccess('Se ha guardado el registro.');

        }elseif ($facture->purse == 'Incentivos') {
            $facture->purse = 'incentives';
            $facture->total = $request->comision + $request->monto_fondeo;
            $facture->save();
            return redirect()->action('TotalFacturesController@index')->withSuccess('Se ha guardado el registro.');

        }elseif ($facture->purse == 'Viaticos') {
            $facture->purse = 'travel_expensive';
            $facture->total = $request->comision + $request->monto_fondeo;
            $facture->save();
            return redirect()->action('TotalFacturesController@index')->withSuccess('Se ha guardado el registro.');

        }
        return redirect()->action('TotalFacturesController@index')->withDanger('Hubo un error');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {

    }
}
