<?php

namespace ZurichMonederos\Http\Controllers\AssigmentsCancellations;

use Illuminate\Http\Request;
use ZurichMonederos\Http\Controllers\Controller;
use ZurichMonederos\PurseGas;

class AssigmentsGasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     $pursesNotAssign = PurseGas::all()->where('nombre_trabajador', null)->where('estatus','Disponible');
     $purses = PurseGas::all()->whereIn('estatus', ['Entregada', 'Cancelada']);

     return view('assigmentsCancellations.gas.index', compact('purses','pursesNotAssign'));
 }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pursegas = new PurseGas($request->all());
        $pursegas->save();
        return redirect()->action('assigmentsCancellations\AssigmentsGasController@index')->withSuccess('Se ha guardado la asignación');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pursegas = PurseGas::find($id);
        $pursegas->fill($request->all());
        $pursegas->estatus = 'Entregada';
        $pursegas->save();
        return redirect()->action('assigmentsCancellations\AssigmentsGasController@index')->withSuccess('Se ha guardado la asignación');
    }


    /**
     * Remove the specified resource from storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {   
        $pursegas = PurseGas::find($id);
        $pursegas->fill($request->all());
        $pursegas->estatus = 'Cancelada';
         $pursegas->nombre_trabajador = null;
        // $pursegas->empleado = 'Sin Asignar';
        $pursegas->save();
        return redirect()->action('assigmentsCancellations\AssigmentsGasController@index')->withSuccess('Se ha guardado la Cancelación');
    }
}
