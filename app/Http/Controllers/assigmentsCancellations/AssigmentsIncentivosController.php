<?php

namespace ZurichMonederos\Http\Controllers\AssigmentsCancellations;

use Illuminate\Http\Request;
use ZurichMonederos\Http\Controllers\Controller;
use ZurichMonederos\PurseIncentive;

class AssigmentsIncentivosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
       $pursesNotAssign = PurseIncentive::all()->where('nombre_trabajador', null)->where('estatus','Disponible');
       $purses = PurseIncentive::all()->whereIn('estatus', ['Entregado', 'Cancelado','Enviado']);
       return view('assigmentsCancellations.incentivos.index', compact('purses','pursesNotAssign'));
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $purseincentive = new PurseIncentive($request->all());
        $purseincentive->save();
        return redirect()->action('assigmentsCancellations\AssigmentsIncentivosController@index')->withSuccess('Se ha guardado la asignación');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $purseincentive = PurseIncentive::find($id);
        $purseincentive->fill($request->all());
        $purseincentive->estatus = 'Enviado';
        $purseincentive->save();
        return redirect()->action('assigmentsCancellations\AssigmentsIncentivosController@index')->withSuccess('Se ha guardado la asignación');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
       $purseincentive = PurseIncentive::find($id);
       $purseincentive->fill($request->all());
       $purseincentive->estatus = 'Cancelado';
       $purseincentive->nombre_trabajador = null;
       $purseincentive->save();
       return redirect()->action('assigmentsCancellations\AssigmentsIncentivosController@index')->withSuccess('Se ha guardado la Cancelación');
   }
}
