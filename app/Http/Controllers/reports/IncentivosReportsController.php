<?php

namespace ZurichMonederos\Http\Controllers\Reports;

use Illuminate\Http\Request;
use ZurichMonederos\Http\Controllers\Controller;
use ZurichMonederos\PurseIncentive;
use Maatwebsite\Excel\Facades\Excel;
use File;

class IncentivosReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $purses = PurseIncentive::all();
        if ($request->has('name_trabajador') && $request->name_trabajador) {
            $purses = PurseIncentive::where('nombre_trabajador', 'like', '%' .  $request->name_trabajador. '%')->get();
        }
        if ($request->has('identificador_filter') && $request->identificador_filter) {
            $purses = PurseIncentive::where('identificador','like', '%' . $request->identificador_filter. '%' )->get();
        }
        if ($request->has('region') && $request->region) {
            $purses = PurseIncentive::where('region',$request->region )->get();
        }

        return view('reports.incentivos.index', compact('purses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return 
     */
    public function destroy()
    {

    }
     /**
     * File Export Code     
     *
     * @var array
     */

     public function export(Request $request) {
      Excel::create('MONEDEROS_INCENTIVOS', function($excel) use ($request){
        $excel->sheet('REPORTE', function($sheet) use ($request) {
            $MONEDEROS_INCENTIVOS = PurseIncentive::all();
            $sheet->fromArray($explode_id = json_decode($request->report, true));
            $sheet->setOrientation('landscape');
            $sheet->row(1, [
                'NOMBRE TRABAJADOR', 'ID TGS SI VALE', 'IDENTIFICADOR', 'NO TARJETA', 'CONCEPTO','REGION','ESTATUS','FECHA DE ENVIO', 'MONTO DE FONDEO', 'FECHA DE FONDEO','EMPLEADO','MES', 'ANIO'
                ]);
            $sheet->row(1, function($row) { $row->setBackground('#4472c4'); });
            $sheet->row(1, function($row) { $row->setAlignment('center'); });
            $sheet->row(1, function($row) { $row->setFont(array( 'family' => 'Calibri', 'size' => '11', 'bold' => true )); });
            $sheet->getRowDimension(1)->setRowHeight(15);
        });
    })->export('xls');
  }
}
