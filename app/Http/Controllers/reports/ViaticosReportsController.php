<?php

namespace ZurichMonederos\Http\Controllers\Reports;

use Illuminate\Http\Request;
use ZurichMonederos\Http\Controllers\Controller;
use ZurichMonederos\PurseTravelExpense;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use File;

class ViaticosReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $purses = PurseTravelExpense::all();
        if ($request->has('name_trabajador') && $request->name_trabajador) {
            $purses = PurseTravelExpense::where('nombre_trabajador', 'like', '%' .  $request->name_trabajador. '%')->get();
        }
        if ($request->has('identificador_filter') && $request->identificador_filter) {
            $purses = PurseTravelExpense::where('identificador','like', '%' . $request->identificador_filter .'%')->get();

        }
        return view('reports.viaticos.index', compact('purses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response   
     */
    public function destroy()
    {

    }
    /**
     * File Export Code     
     *
     * @var array
     */
    public function export(Request $request) {
      Excel::create('MONEDEROS_VIATICOS', function($excel) use ($request){
        $excel->sheet('REPORTE', function($sheet) use ($request) {
            $MONEDEROS_INCENTIVOS = PurseTravelExpense::all();
            $sheet->fromArray($explode_id = json_decode($request->report, true));
            $sheet->setOrientation('landscape');
            $sheet->row(1,[
                'NOMBRE TRABAJADOR', 'ID TGS SI VALE', 'IDENTIFICADOR', 'NO TARJETA', 'CONCEPTO','ESTATUS','FECHA DE ENVIO','ACUMULADO','SEGMENTO', 'EMPLEADO', 'FOLIO REASIGNACIÓN'
                ]);
            $sheet->row(1, function($row) { $row->setBackground('#4472c4'); });
            $sheet->row(1, function($row) { $row->setAlignment('center'); });
            $sheet->row(1, function($row) { $row->setFont(array( 'family' => 'Calibri', 'size' => '11', 'bold' => true )); });
            $sheet->getRowDimension(1)->setRowHeight(15);
        });
    })->export('xls');
  }

/*    public function export(Request $request) {
        Excel::create('MONEDEROS_VIATICOS', function($excel) use ($request){
            $excel->sheet('REPORTE', function($sheet) use ($request) {
                $purses = PurseTravelExpense::all();
                dd($name = $request->query('name_trabajador'));
                if (Input::has('name_trabajador') && $request->input('name_trabajador')) {
                    $purses = PurseTravelExpense::where('nombre_trabajador', 'like', '%' .  $request->input('name_trabajador'). '%')->get();
                    $sheet->fromArray($purses);
                    $sheet->setOrientation('landscape');
                    $sheet->row(1,[
                        'Nombre Trabajador', 'ID TGS SI VALE', 'Identificador', 'No Tarjeta', 'Concepto','Estatus','Fecha de Envio','Acumulado','Segmento', 'Empleado', 'Folio Reasignación'
                        ]);
                }
                $sheet->fromArray($purses);
                $sheet->setOrientation('landscape');
                $sheet->row(1,[
                    'Nombre Trabajador', 'ID TGS SI VALE', 'Identificador', 'No Tarjeta', 'Concepto','Estatus','Fecha de Envio','Acumulado','Segmento', 'Empleado', 'Folio Reasignación'
                    ]);

            });
        })->export('xls');
    }*/
}
