<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 03/12/17
 * Time: 19:07
 */

namespace ZurichMonederos\Auth;


use Illuminate\Auth\GuardHelpers;
use Illuminate\Auth\TokenGuard;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\Auth;

class WebOrApiGuard implements Guard
{

    use GuardHelpers;

    protected $user;
    protected $provider;
    protected $tokenGuard;

    public function __construct(UserProvider $provider)
    {
        $this->provider = $provider;
        $this->tokenGuard = new TokenGuard($provider, request());
        \Log::debug(request()->session()->all());
    }

    public function check()
    {
        \Log::debug('checking');
        if ($this->user) {
            return true;
        }
        $this->user = $this->user();
        return $this->user !== null;
    }

    public function guest()
    {
        \Log::debug('guest');
        if ($this->user) {
            return false;
        }
        return Auth::guard('web')->guest() && $this->tokenGuard->guest();
    }

    public function user()
    {
        \Log::debug('User');
        if ($this->user) {
            return $this->user;
        }
        $user = Auth::guard('web')->user();
        \Log::debug('User', $user ? $user->toArray() : []);

        if (!$user) {
            $user = $this->tokenGuard->user();
        }
        if ($user) {
            $this->user = $user;
        }
        return $user;
    }

    public function id()
    {
        \Log::debug('id');
        if ($this->user) {
            return $this->user->id;
        }
        $user = Auth::guard('web')->user();
        if ($user) return $user->id;
        $user = $this->tokenGuard->user();
        return $user ? $user->id : null;
    }

    public function validate(array $credentials = [])
    {
        \Log::debug('valiid');
        return Auth::guard('web')->validate($credentials);
    }

    public function setUser(Authenticatable $user)
    {
        \Log::debug('Setting User');
        $this->user = $user;
    }


}
