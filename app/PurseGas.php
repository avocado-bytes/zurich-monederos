<?php

namespace ZurichMonederos;

use Illuminate\Database\Eloquent\Model;

class PurseGas extends Model
{
    	protected $fillable = [
		"nombre_trabajador",
		"id_tgs_si_vale",
		"identificador",
		"no_tarjeta",
		"concepto",
		"estatus",
		"fecha_de_envio",
		"monto_de_fondeo",
		"empleado",
	];

	protected $hidden = [
		'id',
		'created_at',
		'updated_at'
	];
}
           