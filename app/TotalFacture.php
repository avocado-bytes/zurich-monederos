<?php

namespace ZurichMonederos;

use Illuminate\Database\Eloquent\Model;

class TotalFacture extends Model
{
     protected $fillable = [
		"concepto",
		"monto_fondeo",
		"comision",
		"total",
		"fecha_fondeo",
		"mes",
		"numero_pedido",
		"anio",
		"purse",
	];
             
}
