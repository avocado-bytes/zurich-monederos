@extends('app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h1 class="dashboard-heading">
                Disponibilidad
            </h1>
            <h2 class="dashboard-heading">
                Solicitud de Tarjetas
            </h2>
        </div>
        <div class="col-xs-12 col-sm-6 text-right">
            <button type="button" class="btn btn-primary btn-header registrar-cards"  data-toggle="modal" data-target="#charger-manual">
                Registrar informacion <i class="fa fa-plus" aria-hidden="true"></i>
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel with-nav-tabs panel-primary">
                <div class="panel-heading">
                    <ul class="nav nav-tabs">
                        <li class="active" id="Gasolina"><a href="#tab1primary" data-toggle="tab">Gasolina</a></li>
                        <li id="Incentivos"><a href="#tab2primary" data-toggle="tab">Incentivos</a></li>
                        <li id="Viaticos"><a href="#tab3primary" data-toggle="tab">Viaticos</a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1primary">
                            @if ($cardrequestsGas->count() > 0)
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="table-responsive">
                                        <table class="table table-striped big-tableCenter big-table">
                                            <thead>
                                                <tr>
                                                    <th>CANTIDAD</th>
                                                    <th>NO. DE PEDIDO</th>
                                                    <th>FECHA DE SOLICITUD</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($cardrequestsGas as $CardRequest)  
                                                <tr>
                                                    <td><span>{{ $CardRequest->cantidad}}</span></td>
                                                    <td><span>{{ $CardRequest->numero_pedido}}</span></td>
                                                    <td><span>{{ $CardRequest->fecha_solicitud}}</span></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="alert alert-info">
                                No hay registros que mostrar
                            </div>
                            @endif
                        </div>
                        <div class="tab-pane fade" id="tab2primary">
                            @if ($cardrequestsIncentivos->count() > 0)
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="table-responsive">
                                        <table class="table table-striped big-tableCenter big-table">
                                            <thead>
                                                <tr>
                                                    <th>CANTIDAD</th>
                                                    <th>NO. DE PEDIDO</th>
                                                    <th>FECHA DE SOLICITUD</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($cardrequestsIncentivos as $CardRequest)  
                                                <tr>
                                                    <td><span>{{ $CardRequest->cantidad}}</span></td>
                                                    <td><span>{{ $CardRequest->numero_pedido}}</span></td>
                                                    <td><span>{{ $CardRequest->fecha_solicitud}}</span></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="alert alert-info">
                                No hay registros que mostrar
                            </div>
                            @endif
                        </div>
                        <div class="tab-pane fade" id="tab3primary">
                            @if ($cardrequestsViaticos->count() > 0)
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="table-responsive">
                                        <table class="table table-striped big-tableCenter big-table">
                                            <thead>
                                                <tr>
                                                    <th>CANTIDAD</th>
                                                    <th>NO. DE PEDIDO</th>
                                                    <th>FECHA DE SOLICITUD</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($cardrequestsViaticos as $CardRequest)  
                                                <tr>
                                                    <td><span>{{ $CardRequest->cantidad}}</span></td>
                                                    <td><span>{{ $CardRequest->numero_pedido}}</span></td>
                                                    <td><span>{{ $CardRequest->fecha_solicitud}}</span></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="alert alert-info">
                                No hay registros que mostrar
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('modals')
<div class="modal fade" id="charger-manual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
      <!-- Empieza Formulario --> 
      {!! Form::open(array('action' => 'CardRequestController@store')) !!}
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Registrar informacion</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                        {!! Form::label('cantidad', 'CANTIDAD:')!!}
                        {!! Form::text('cantidad', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                        {!! Form::label('numero_pedido', 'NO. DE PEDIDO:')!!}
                        {!! Form::text('numero_pedido', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                        {!! Form::label('fecha_solicitud', 'FECHA DE SOLICITUD:')!!}
                        {!! Form::date('fecha_solicitud', Carbon\Carbon::today()->format('Y-m-d'), ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                        {!! Form::label('purse', 'Compensación:')!!}
                        {!! Form::text('purse', null, ['class' => 'form-control','readonly' => 'readonly']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button class="btn btn-primary" type="submit">Guardar</button>
        </div>
        {!! Form::close() !!}
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $(".registrar-cards").click(function() {
            var active = $("ul.nav li.active").attr("id");
            $("#purse").val(active);

        });
    });
</script>
@endsection