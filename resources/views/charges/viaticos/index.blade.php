@extends('app')


@section('content')

<div class="container">
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <p>Corrige los siguientes errores:</p>
    <ul>
      @foreach ($errors->all() as $message)
      <li>{{ $message }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <div class="row">
    <div class="col-sm-8">
     <h1 class="dashboard-heading">
      Carga de Información
    </h1>
    <h2 class="dashboard-heading">
      Viáticos
    </h2>
  </div>
  <div class="col-sm-2 text-right">
    <button type="button" class="btn btn-primary btn-header" data-toggle="modal" data-target="#charger-manual">
      Cargar Manual <i class="fa fa-plus" aria-hidden="true"></i>
    </button>
  </div>
  <div class="col-sm-2 text-right">
    <button type="button" class="btn btn-primary btn-header" data-toggle="modal" data-target="#charger-excel">
      Cargar Excel <i class="fas fa-file-excel" aria-hidden="true"></i>
    </button>
  </div>
</div>
@if ($purses->count() > 0)
<div class="table-responsive">
  <table class="table table-striped big-table">
    <thead>
      <tr>
        <th>NOMBRE DEL TRABAJADOR</th>
        <th>ID TGS SI VALE</th>
        <th>IDENTIFICADOR</th>
        <th>NO. TARJETA</th>
        <th>CONCEPTO</th>
        <th>ESTATUS</th>
        <th>FECHA DE ENVIO</th>
        <th>ACUMULADO</th>
        <th>SEGMENTO</th>
        <th>NUMERO DE EMPLEADO</th>
        <th>FOLIO DE REASIGNACION</th>
      </tr>
    </thead>
    <tbody>
      @foreach($purses as $PurseTravelExpense)  
      <tr>
       <td><span>{{ $PurseTravelExpense->nombre_trabajador }}</span></td>
       <td><span>{{ $PurseTravelExpense->id_tgs_si_vale }}</span></td>
       <td><span>{{ $PurseTravelExpense->identificador }}</span></td>
       <td><span>{{ number_format($PurseTravelExpense->no_tarjeta,0,'.','') }}</span></td>
       <td><span>{{ $PurseTravelExpense->concepto }}</span></td>
       <td><span>{{ $PurseTravelExpense->estatus }}</span></td>
       <td><span>{{ $PurseTravelExpense->fecha_de_envio }}</span></td>
       <td><span><span>$</span>{{ number_format($PurseTravelExpense->acumulado) }}</span><span>.00</span></td>
       <td><span>{{ $PurseTravelExpense->segmento }}</span></td>
       <td><span>{{ $PurseTravelExpense->empleado }}</span></td>
       <td><span>{{ $PurseTravelExpense->folio_reasignacion }}</span></td>
     </tr>
     @endforeach
   </tbody>
 </table>
</div>
@else
<div class="alert alert-info">
  No hay registros que mostrar
</div>
@endif
</div>

@endsection
@section('modals')
<div class="modal fade" id="charger-manual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
     <!-- Empieza Formulario --> 
     {!! Form::open(array('action' => "Charges\ViaticosChargesController@store")) !!}
     <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Carga Manual</h4>
    </div>
    <div class="modal-body">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            {!! Form::label('nombre_trabajador', 'NOMBRE DEL TRABAJADOR:')!!}
            {!! Form::text('nombre_trabajador', null, ['class' => 'form-control']) !!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('id_tgs_si_vale', 'ID TGS SI VALE:')!!}
            {!! Form::text('id_tgs_si_vale', null, ['class' => 'form-control']) !!}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('no_tarjeta', 'NO. TARJETA:')!!}
            {!! Form::text('no_tarjeta', null, ['class' => 'form-control']) !!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('identificador', 'IDENTIFICADOR:')!!}
            {!! Form::text('identificador', null, ['class' => 'form-control']) !!}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('concepto', 'CONCEPTO:')!!}
            {!! Form::text('concepto', 'Viáticos', ['class' => 'form-control','readonly' => 'readonly']) !!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
         <div class="form-group">
          {!! Form::label('estatus', 'ESTATUS:')!!}
          {!! Form::select('estatus', ['Disponible' => 'Disponible', 'Entregada' => 'Entregada', 'Cancelada' => 'Cancelada'], 'Disponible', ['class' => 'form-control']) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('fecha_de_envio', 'FECHA DE ENVIO', ['class' => 'control-label']) !!}
          {!! Form::date('fecha_de_envio', Carbon\Carbon::today()->format('Y-m-d'), ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus']) !!}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('acumulado', 'ACUMULADO:')!!}
          <div class="input-group">
            <div class="input-group-addon">$</div>
            {!! Form::text('acumulado', null, ['class' => 'form-control']) !!}
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('segmento', 'SEGMENTO:')!!}
          {!! Form::text('segmento', null, ['class' => 'form-control']) !!}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          @isset($last)
          {!! Form::label('empleado', 'NOMBRE DE EMPLEADO:')!!}
          {!! Form::text('empleado', 'Viaticos ', ['class' => 'form-control']) !!}
          @endisset
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('folio_reasignacion', 'FOLIO DE REASIGNACIÓN:')!!}
          {!! Form::text('folio_reasignacion', null, ['class' => 'form-control']) !!}
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
    <button class="btn btn-primary" type="submit">Guardar</button>
  </div>
</div>
<!-- Termina Formulario -->
{!! Form::close() !!}
<!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="charger-excel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Carga de Archivo excel</h4>
      </div>
      <div class="panel-body">

        <!-- Standar Form -->
        <!-- Standar Form -->
        <h4>Selecciona los archivos a subir con extensión .xlsx</h4>
        <form action="ImportPurseTravel" method="post" enctype="multipart/form-data" id="js-upload-form">
          <div class="form-inline">
            <div class="form-group">
              <input type="file" name="file" id="js-upload-files">
              <input type="hidden" value="{{ csrf_token() }}" name="_token">
            </div>
 <!--            <button type="submit" class="btn btn-sm btn-primary" value="Upload" id="js-upload-submit">Cargar archivo</button> -->
          </div>

     <!--    <h4>O arrastra y suelta el archivo aquí</h4>
        <div class="upload-drop-zone" id="drop-zone">
          Solo arrastra y suelta los archivos aquí
        </div>

        <div class="progress">
         <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
           60%
         </div>
       </div>

       <div class="js-upload-finished">
        <h3>Archivos Procesados</h3>
        <div class="list-group">
          <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">Exito</span>archivo-01.xlsx</a>
          <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">Exito</span>archivo-02.xlsx</a>
        </div>
      </div> -->
    </div>
    <div class="modal-footer">
      <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
      <button type="submit" class="btn btn-sm btn-primary" value="Upload" id="js-upload-submit">Cargar archivo</button>
    </div>
  </form>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

