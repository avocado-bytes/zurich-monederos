@extends('app')


@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-6">
			<h1 class="dashboard-heading">
				Reportes
			</h1>
			<h2 class="dashboard-heading">
				Incentivos
			</h2>
		</div>
		@if ($purses->count() > 0)
		<div class="col-md-6 text-right" style="padding-top: 29px">
		{!! Form::open(['method' => 'POST','url' => url('excelincentivos') ]) !!}
            <div class="form-group">
                <button class="btn btn-primary export-button" type="submit">
                    Exportar Reporte <i class="fas fa-file-alt"></i>
                </button>
            </div>
            <input type="hidden" name="report" value='{!! json_encode($purses->toArray()) !!}'>
            {!! Form::close() !!}
		</div>
		@else

		@endif
	</div>
	@if ($purses->count() > 0)
	{!! Form::open([
		'method' => 'GET',
		]) !!}
		<div class="row">
			<div class="col-sm-2">
				<div class="form-group">
					{!! Form::label('since', 'Desde') !!}
					{!! Form::date('since', \Carbon\Carbon::today()->format('Y-m-d'), ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					{!! Form::label('unitl', 'Hasta') !!}
					{!! Form::date('until', \Carbon\Carbon::today()->endOfMonth()->format('Y-m-d'), ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					{!! Form::label('region', 'Filtrar por Regiones') !!}
					{!! Form::select('region', array_merge([null => 'Todos'], ZurichMonederos\PurseIncentive::REGIONES), request()->input('region'), ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="filter-nombre">
				<div class="col-sm-2">
					<div class="form-group ">
						<label for="filter-name">Filtrar por Nombre</label>
						{!! Form::text('name_trabajador', request()->input('name_trabajador'), ['class' => 'form-control', 'id' => 'filter-name']) !!}
					</div>
				</div>
			</div>
			<div class="filter-identificador">
				<div class="col-sm-2">
					<div class="form-group">
						<label for="filter_identificador">Filtrar por Identificador</label>
						{!! Form::text('identificador_filter', request()->input('identificador_filter'), ['class' => 'form-control', 'id' => 'filter_identificador']) !!}
					</div>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="">&nbsp;</label>
							<button id="filter-btn" type="submit" class="btn btn-primary btn-block">
								<i class="fa fa-fw fa-search"></i> Filtrar
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		{!! Form::close() !!}

		<div class="table-responsive">
			<table class="table table-striped big-table">
				<thead>
					<tr>
						<th>NOMBRE DEL TRABAJADOR</th>
						<th>ID TGS SI VALE</th>
						<th>IDENTIFICADOR</th>
						<th>NO. TARJETA</th>
						<th>CONCEPTO</th>
						<th>REGIONES</th>
						<th>ESTATUS</th>
						<th>FECHA DE ENVIO</th>
						<th>MONTO DE FONDEO</th>
						<th>FECHA DE FONDEO</th>
						<th>NOMBRE EMPLEADO</th>
						<th>MES</th>
						<th>AÑO</th>
					</tr>
				</thead>
				<tbody>
					@foreach($purses as $PurseIncentive)  
					<tr>
						<td><span>{{ $PurseIncentive->nombre_trabajador}}</span></td>
						<td><span>{{ $PurseIncentive->id_tgs_si_vale }}</span></td>
						<td><span>{{ $PurseIncentive->identificador }}</span></td>
						<td><span>{{ number_format($PurseIncentive->no_tarjeta,0,'.','') }}</span></td>
						<td><span>{{ $PurseIncentive->concepto }}</span></td>
						<td><span>{{ $PurseIncentive->region }}</span></td>
						<td><span>{{ $PurseIncentive->estatus }}</span></td>
						<td><span>{{ $PurseIncentive->fecha_de_envio }}</span></td>
						<td><span>$</span><span>{{ number_format($PurseIncentive->monto_de_fondeo) }}</span><span>.00</span></td>
						<td><span>{{ $PurseIncentive->fecha_de_fondeo }}</span></td>
						<td><span>{{ $PurseIncentive->empleado }}</span></td>
						<td><span>{{ $PurseIncentive->mes }}</span></td>
						<td><span>{{ $PurseIncentive->anio }}</span></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		@else
		<div class="alert alert-info">
			No hay registros que mostrar
		</div>
		@endif
	</div>

	@endsection

