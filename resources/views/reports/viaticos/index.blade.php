@extends('app')


@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-6">
			<h1 class="dashboard-heading">
				Reportes
			</h1>
			<h2 class="dashboard-heading">
				Viáticos
			</h2>
		</div>
		@if ($purses->count() > 0)
		<div class="col-md-6 text-right" style="padding-top: 29px">
		{!! Form::open(['method' => 'POST','url' => url('excelviaticos') ]) !!}
            <div class="form-group">
                <button class="btn btn-primary export-button" type="submit">
                    Exportar Reporte <i class="fas fa-file-alt"></i>
                </button>
            </div>
            <input type="hidden" name="report" value='{!! json_encode($purses->toArray()) !!}'>
            {!! Form::close() !!}
		</div>
		@else

		@endif
	</div>
	@if ($purses->count() > 0)
	{!! Form::open([
		'method' => 'GET',
		]) !!}
		<div class="row">
			<div class="col-sm-2">
				<div class="form-group">
					{!! Form::label('since', 'Desde') !!}
					{!! Form::date('since', \Carbon\Carbon::today()->format('Y-m-d'), ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					{!! Form::label('unitl', 'Hasta') !!}
					{!! Form::date('until', \Carbon\Carbon::today()->endOfMonth()->format('Y-m-d'), ['class' => 'form-control']) !!}
				</div>
			</div>
			<div class="filter-nombre">
				<div class="col-sm-2">
					<div class="form-group ">
						<label for="filter-name">Filtrar por Nombre</label>
						{!! Form::text('name_trabajador', request()->input('name_trabajador'), ['class' => 'form-control', 'id' => 'filter-name']) !!}
					</div>
				</div>
			</div>
			<div class="filter-identificador">
				<div class="col-sm-2">
					<div class="form-group">
						<label for="filter_identificador">Filtrar por Identificador</label>
						{!! Form::text('identificador_filter', request()->input('identificador_filter'), ['class' => 'form-control', 'id' => 'filter_identificador']) !!}
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label for="">&nbsp;</label>
							<button id="filter-btn" type="submit" class="btn btn-primary btn-block">
								<i class="fa fa-fw fa-search"></i> Filtrar
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		{!! Form::close() !!}
		<div class="table-responsive">
			<table class="table table-striped big-table">
				<thead>
					<tr>
						<th>NOMBRE DEL TRABAJADOR</th>
						<th>ID TGS SI VALE</th>
						<th>IDENTIFICADOR</th>
						<th>NO. TARJETA</th>
						<th>CONCEPTO</th>
						<th>ESTATUS</th>
						<th>FECHA DE ENVIO</th>
						<th>ACUMULADO</th>
						<th>SEGMENTO</th>
						<th>NUMERO DE EMPLEADO</th>
						<th>FOLIO DE REASIGNACION</th>
					</tr>
				</thead>
				<tbody>
					@foreach($purses as $PurseTravelExpense)  
					<tr>
						<td><span>{{ $PurseTravelExpense->nombre_trabajador }}</span></td>
						<td><span>{{ $PurseTravelExpense->id_tgs_si_vale }}</span></td>
						<td><span>{{ $PurseTravelExpense->identificador }}</span></td>
						<td><span>{{ $PurseTravelExpense->no_tarjeta }}</span></td>
						<td><span>{{ $PurseTravelExpense->concepto }}</span></td>
						<td><span>{{ $PurseTravelExpense->estatus }}</span></td>
						<td><span>{{ $PurseTravelExpense->fecha_de_envio }}</span></td>
						<td><span>$</span><span>{{ number_format($PurseTravelExpense->acumulado) }}</span><span>.00</span></td>
						<td><span>{{ $PurseTravelExpense->segmento }}</span></td>
						<td><span>{{ $PurseTravelExpense->empleado }}</span></td>
						<td><span>{{ $PurseTravelExpense->folio_reasignacion }}</span></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		@else
		<div class="alert alert-info">
			No hay registros que mostrar
		</div>
		@endif
	</div>
	@endsection

