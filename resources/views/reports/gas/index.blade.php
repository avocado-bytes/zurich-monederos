@extends('app')
@section('content')



<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h1 class="dashboard-heading">
                Reportes
            </h1>
            <h2 class="dashboard-heading">
                Gasolina
            </h2>
        </div>
        @if ($purses->count() > 0)
        <div class="col-md-6 text-right" style="padding-top: 29px">
            
            {!! Form::open(['method' => 'POST','url' => url('excel') ]) !!}
            <div class="form-group">
                <button class="btn btn-primary export-button" type="submit">
                    Exportar Reporte <i class="fas fa-file-alt"></i>
                </button>
            </div>
            <input type="hidden" name="report" value='{!! json_encode($purses->toArray()) !!}'>
            {!! Form::close() !!}
        </div>
        @else

        @endif
    </div>
    @if ($purses->count() > 0)
    {!! Form::open([
    'method' => 'GET',
    ]) !!}
    <div class="row">
        <div class="col-sm-2">
            <div class="form-group">
                {!! Form::label('since', 'Desde') !!}
                {!! Form::date('since', \Carbon\Carbon::today()->format('Y-m-d'), ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                {!! Form::label('unitl', 'Hasta') !!}
                {!! Form::date('until', \Carbon\Carbon::today()->endOfMonth()->format('Y-m-d'), ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="filter-nombre">
            <div class="col-sm-2">
                <div class="form-group ">
                    <label for="filter-name">Filtrar por Nombre</label>
                    {!! Form::text('name_trabajador', request()->input('name_trabajador'), ['class' => 'form-control', 'id' => 'filter-name']) !!}
                </div>
            </div>
        </div>
        <div class="filter-identificador">
            <div class="col-sm-2">
                <div class="form-group">
                    <label for="filter_identificador">Filtrar por Identificador</label>
                    {!! Form::text('identificador_filter', request()->input('identificador_filter'), ['class' => 'form-control', 'id' => 'filter_identificador']) !!}
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="">&nbsp;</label>
                        <button id="filter-btn" type="submit" class="btn btn-primary btn-block">
                            <i class="fa fa-fw fa-search"></i> Filtrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <div class="table-responsive">
        <table class="table big-table table-striped table-hover big-table">
            <thead>
                <tr>
                    <th>NOMBRE DEL TRABAJADOR</th>
                    <th>ID TGS SI VALE</th>
                    <th>IDENTIFICADOR</th>
                    <th>NO. TARJETA</th>
                    <th>CONCEPTO</th>
                    <th>ESTATUS</th>
                    <th>FECHA DE ENVIO</th>
                    <th>MONTO DE FONDEO</th>
                    <th>NUMERO DE EMPLEADO</th>
                </tr>
            </thead>
            <tbody>
                @foreach($purses as $PurseGas)  
                <tr>
                    <td><span>{{ $PurseGas->nombre_trabajador}}</span></td>
                    <td><span>{{ $PurseGas->id_tgs_si_vale }}</span></td>
                    <td><span>{{ $PurseGas->identificador }}</span></td>
                    <td><span>{{ number_format($PurseGas->no_tarjeta,0,'.','') }}</span></td>
                    <td><span>{{ $PurseGas->concepto }}</span></td>
                    <td><span>{{ $PurseGas->estatus }}</span></td>
                    <td><span>{{ $PurseGas->fecha_de_envio }}</span></td>
                    <td><span>$</span><span>{{ number_format($PurseGas->monto_de_fondeo) }}</span><span>.00</span></td>
                    <td><span>{{ $PurseGas->empleado }}</span></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @else
    <div class="alert alert-info">
        No hay registros que mostrar
    </div>
    @endif
</div>

@endsection
@section('script')
<!-- 	<script>
        $(function() {
                $('.filter-nombre').show();
                $('.filter-identificador').hide();
                $('#filtro').change(function(){
                        if($(this).val() == 'name') {
                                $('.filter-nombre').show();
                                $('.filter-identificador').hide(); 
                        } else if ($(this).val() == 'identificador') {
                                $('.filter-nombre').hide();
                                $('.filter-identificador').show(); 
                        }else {
                                $('.filter-nombre').hide();
                                $('.filter-identificador').hide(); 
                        } 
                });
        });
</script> -->
<!-- <script>
$(document).ready(function() {
        $(".export-button").click(function() {
                var data = $('.big-table').serialize();
                console.log(data);

                $.ajaxSetup({
                        headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        }
                });
                $.ajax({
                        url: 'http://localhost:8000/excelgas',
                        dataType : 'json', 
                        type: 'POST',
                        data: {
                                active:active, 
                        },
                        success: function(data) {
                                console.log("Success with data " + data);
                        },
                        error: function(data) {
                                console.log("Error with data " + data);
                        }
                });
        });
});
</script> -->
@endsection