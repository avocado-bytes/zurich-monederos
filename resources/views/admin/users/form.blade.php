{!! Form::model($user, [
  'action' => $user->exists ? ['Admin\UsersController@update', $user] : 'Admin\UsersController@store',
  'method' => $user->exists ? 'PUT' : 'POST',
]) !!}

  <div class="form-group">
    <label for="first_name">Nombre(s)</label>
    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
  </div>

  <div class="form-group">
    <label for="last_name">Apellido(s)</label>
    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
  </div>

  <div class="form-group">
    <label for="email">Correo electrónico</label>
    {!! Form::text('email', null, ['class' => 'form-control', 'readonly' => $user->exists ? 'readonly' : null]) !!}
  </div>

  <div class="form-group">
    <label for="cuenta_z">Cuenta Z</label>
    {!! Form::text('cuenta_z', null, ['class' => 'form-control']) !!}
  </div>

  <div class="form-group">
    <label for="role">Perfil</label>
    {!! Form::select('role', ZurichMonederos\User::ROLES, null, [ 'class' => 'form-control' ]) !!}
  </div>

  @if(!$user->exists)
    <div class="form-group">
      <label for="password">Contraseña</label>
      {!! Form::password('password', ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
      <label for="password_confirmation">Confirmar contraseña</label>
      {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
    </div>
  @endif

  <div class="text-center">
    <a href="{{ URL::previous() ?: action('Admin\UsersController@index') }}" class="btn btn-danger">
      Cancelar
    </a>
    <button type="submit" class="btn btn-primary">
      Guardar
    </button>
  </div>

{!! Form::close() !!}
