@extends('app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <h1>Crear usuario</h1>
        @include('admin.users.form')
      </div>
    </div>
  </div>
@endsection
