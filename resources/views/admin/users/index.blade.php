@extends("app")


@section("content")

  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <h1 class="h1">Usuarios</h1>
      </div>
      <div class="col-xs-12 col-sm-6 text-right">
        <a href="{{ action('Admin\UsersController@create') }}" class="btn btn-primary btn-header">
          Nuevo usuario <i class="fa fa-user-plus" aria-hidden="true"></i>
        </a>
      </div>
    </div>
    {!! Form::open([
      'method' => 'GET',
    ]) !!}
      <div class="row">
        <div class="col-xs-12 col-sm-3">
          <div class="form-group">
            <label for="filter-area">Filtrar por Área</label>
            {!! Form::select('role', array_merge([null => 'Todos'], ZurichMonederos\User::ROLES), request()->input('role'), ['class' => 'form-control', 'id' => 'filter-area']) !!}
          </div>
        </div>
        <div class="col-xs-12 col-sm-3">
          <div class="form-group">
            <label for="filter-name">Filtrar por Nombre</label>
            {!! Form::text('name', request()->input('name'), ['class' => 'form-control', 'id' => 'filter-name']) !!}
          </div>
        </div>
        <div class="col-xs-12 col-sm-2">
          <button type="submit" class="btn btn-primary btn-block btn-input">Filtrar</button>
        </div>
      </div>
    {!! Form::close() !!}
    @if ($users->count() > 0)
      <div class="table-responsive">
        <table class="table table-striped table-hover">
          <thead>
          <tr>
            <th>Nombre Completo</th>
            <th>Correo</th>
            <th>Cuenta Z</th>
            <th>Área</th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          @foreach($users as $user)
            <tr>
              <td class="user-name">
                {{ $user->first_name }} {{ $user->last_name }}
              </td>
              <td>
                {{ $user->email }}
              </td>
              <td>
                {{ $user->cuenta_z }}
              </td>
              <td>
                {{ ZurichMonederos\User::ROLES[$user->role] }}
              </td>
              <td>
                <a href="{{ action('Admin\UsersController@edit', $user) }}" class="btn btn-default">
                  <i class="far fa-edit"></i>
                </a>
                <button
                  class="btn btn-danger btn-delete-user"
                  data-target="#delete-user-modal"
                  data-toggle="modal"
                  data-action="{{action('Admin\UsersController@destroy', $user)}}"
                >
                  <i class="fa fa-fw fa-trash"></i>
                </button>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        {!! $users->appends(request()->all())->links() !!}
      </div>
    @else
      <div class="alert alert-info">
        No hay usuarios que mostrar
      </div>
    @endif

  </div>

@endsection

@section('modals')
  <div class="modal fade" id="delete-user-modal">
    {!! Form::open([
      'id' => 'delete-user-form',
      'method' => 'DELETE',
      'class' => 'modal-dialog modal-sm',
    ]) !!}
      <div class="modal-content">
        <div class="modal-body">
          ¿Está seguro que desea dar de baja al usuario <strong id="delete-user-name"></strong>?
        </div>
        <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-danger">Sí, dar de baja</button>
        </div>
      </div>
    {!! Form::close() !!}
  </div>
@endsection

@section('script')
  <script>
    $(function() {
      $('.btn-delete-user').click(function() {
        var action = $(this).attr('data-action');
        console.log(action);
        $('#delete-user-form').attr('action', action);
        $('#delete-user-name').text($(this).parents('tr').find('.user-name').text());
      });
    });
  </script>
@endsection
