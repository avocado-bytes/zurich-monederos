@extends("app", ['bodyClass' => 'body-login'])

@section("content")
  <div class="container login-container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        {!! Form::open([
            'route' => 'login',
            'method' => 'POST',
            'class' => 'login-form'
        ]) !!}

          <img class="img-logo" src="/images/logo-login.png" alt="">

          @if($errors->has('email'))
            <div class="alert alert-danger">
              {{ $errors->first('email') }}
            </div>
          @endif
          <div class="form-group">
            {!! Form::label('email', 'Correo electrónico') !!}
            {!! Form::email('email', old('email'), ['class' => 'form-control']) !!}
          </div>
          <div class="form-group">
            {!! Form::label('password', 'Contraseña') !!}
            {!! Form::password('password', ['class' => 'form-control']) !!}
          </div>

          <div class="form-group">
            <div class="checkbox">
              <label>
                {!! Form::checkbox('remember', true, old('checked')) !!} Recordarme
              </label>
            </div>
          </div>
          <button type="submit" class="btn btn-primary btn-block u-mb20">
            Iniciar Sesión
          </button>
          <p class="message">
            <a href="{{ route('password.request') }}">Olvidé mi contraseña</a>
          </p>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
@endsection
