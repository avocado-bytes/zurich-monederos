@extends('app')


@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1 class="dashboard-heading">
				Asignaciones y Cancelaciones
			</h1>
			<h2 class="dashboard-heading">
				Incentivos
			</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel with-nav-tabs panel-primary">
				<div class="panel-heading">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab1primary" data-toggle="tab">Asignaciones</a></li>
						<li><a href="#tab2primary" data-toggle="tab">Cancelaciones</a></li>
					</ul>
				</div>
				<div class="panel-body" style="padding: 0px">
					<!-- Empieza Contenido tabs -->
					<div class="tab-content">
						<div class="tab-pane fade in active" id="tab1primary">
							@if ($pursesNotAssign->count() > 0)
							<div class="table-responsive">
								<table class="table table-striped big-table">
									<thead>
										<tr>
											<th>Asignar</th>
											<th>NOMBRE DEL TRABAJADOR</th>
											<th>ID TGS SI VALE</th>
											<th>IDENTIFICADOR</th>
											<th>NO. TARJETA</th>
											<th>CONCEPTO</th>
											<th>REGIONES</th>
											<th>ESTATUS</th>
											<th>FECHA DE ENVIO</th>
											<th>MONTO DE FONDEO</th>
											<th>FECHA DE FONDEO</th>
											<th>NOMBRE EMPLEADO</th>
											<th>MES</th>
											<th>AÑO</th>
										</tr>
									</thead>
									<tbody> 
										@foreach($pursesNotAssign as $PurseIncentive)  
										<tr>
											<td>
												<button type="button" data-toggle="modal" data-target="#asignar{{ $PurseIncentive->id }}">
													<i class="fas fa-user-plus"></i>
												</button>
											</td>
											<td><span>{{ $PurseIncentive->nombre_trabajador}}</span></td>
											<td><span>{{ $PurseIncentive->id_tgs_si_vale }}</span></td>
											<td><span>{{ $PurseIncentive->identificador }}</span></td>
											<td><span>{{ $PurseIncentive->no_tarjeta }}</span></td>
											<td><span>{{ $PurseIncentive->concepto }}</span></td>
											<td><span>{{ $PurseIncentive->region }}</span></td>
											<td><span>{{ $PurseIncentive->estatus }}</span></td>
											<td><span>{{ $PurseIncentive->fecha_de_envio }}</span></td>
											<td><span>$</span><span>{{ number_format($PurseIncentive->monto_de_fondeo)}}</span><span>.00</span></td>
											<td><span>{{ $PurseIncentive->fecha_de_fondeo }}</span></td>
											<td><span>{{ $PurseIncentive->empleado }}</span></td>
											<td><span>{{ $PurseIncentive->mes }}</span></td>
											<td><span>{{ $PurseIncentive->anio }}</span></td>

										</tr>
										<div class="modal fade" id="asignar{{ $PurseIncentive->id }}" tabindex="-1" role="dialog">
											<div class="modal-dialog">
												{!! Form::model($pursesNotAssign, [
												'action' => $PurseIncentive->exists ? ['assigmentsCancellations\AssigmentsIncentivosController@update', $PurseIncentive->id] : 'assigmentsCancellations\AssigmentsIncentivosController@update',$PurseIncentive->id,
												'method' => $PurseIncentive->exists ? 'PUT' : 'POST',
												]) !!}
												<div class="modal-content">
													<!-- Empieza Formulario --> 
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
														<h4 class="modal-title">Asignacion Incentivos</h4>
													</div>
													<div class="modal-body">
														<div class="row">
															<div class="col-md-12">
																<div class="form-group">
																	{!! Form::label('nombre_trabajador', 'NOMBRE DEL TRABAJADOR:')!!}
																	{!! Form::text('nombre_trabajador', $PurseIncentive->nombre_trabajador, ['class' => 'form-control']) !!}
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	{!! Form::label('id_tgs_si_vale', 'ID TGS SI VALE::')!!}
																	{!! Form::text('id_tgs_si_vale', $PurseIncentive->id_tgs_si_vale, ['class' => 'form-control']) !!}
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	{!! Form::label('no_tarjeta', 'NO. TARJETA:')!!}
																	{!! Form::text('no_tarjeta', $PurseIncentive->no_tarjeta, ['class' => 'form-control']) !!}
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	{!! Form::label('identificador', 'IDENTIFICADOR:')!!}
																	{!! Form::text('identificador', $PurseIncentive->identificador, ['class' => 'form-control']) !!}
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	{!! Form::label('concepto', 'CONCEPTO:')!!}
																	{!! Form::text('concepto', $PurseIncentive->concepto, ['class' => 'form-control']) !!}
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	{!! Form::label('region', 'REGION:')!!}
																	{!! Form::select('region', ['Corporativo Call Center' => 'Corporativo Call Center', 'Corporativo Asesores' => 'Corporativo Asesores','Corporativo'=>'Corporativo','Metro Sur'=>'Metro Sur','Metro Norte'=>'Metro Norte','Norte'=>'Norte','Noreste'=>'Noreste','Noroeste'=>'Noroeste','Centro'=>'Centro','Sur'=>'Sur','Sureste'=>'Sureste','Occidente'=>'Occidente','Sin Región'=>'Sin Región'], $PurseIncentive->region,['class' => 'form-control'])!!}
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	{!! Form::label('estatus', 'ESTATUS:')!!}
																	{!! Form::select('estatus', ['Disponible' => 'Disponible', 'Cancelado' => 'Cancelado', 'Enviado' => 'Enviado'], $PurseIncentive->estatus, ['class' => 'form-control']) !!}
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	{!! Form::label('monto_de_fondeo', 'MONTO DE FONDEO:')!!}
																	{!! Form::text('monto_de_fondeo', $PurseIncentive->monto_de_fondeo, ['class' => 'form-control']) !!}
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	{!! Form::label('fecha_de_fondeo', 'FECHA DE FONDEO', ['class' => 'control-label']) !!}
																	{!! Form::date('fecha_de_fondeo', $PurseIncentive->fecha_de_fondeo, ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus']) !!}
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	{!! Form::label('fecha_de_envio', 'FECHA DE ENVIO', ['class' => 'control-label']) !!}
																	{!! Form::date('fecha_de_envio', $PurseIncentive->fecha_de_envio, ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus']) !!}
																</div>
															</div>
															<div class="col-md-3">
																<div class="form-group">
																	{!! Form::label('mes', 'MES:') !!}
																	<!-- selectMonth -->
																	{!! Form::select('mes', ['Enero' => 'Enero', 'Febrero' => 'Febrero','Marzo' => 'Marzo','Abril' => 'Abril','Mayo' => 'Mayo','Junio' => 'Junio','Julio' => 'Julio','Agosto' => 'Agosto','Septiembre' => 'Septiembre','Octubre' => 'Octubre','Novimebre' => 'Noviembre','Diciembre' => 'Diciembre'], $PurseIncentive->mes,['class' => 'form-control']) !!}
																</div>
															</div>
															<div class="col-md-3">
																<div class="form-group">
																	{!! Form::label('anio', 'AÑO:')!!}
																	{!! Form::text('anio', $PurseIncentive->anio, ['class' => 'form-control']) !!}
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	{!! Form::label('empleado', 'NUMERO DE EMPLEADO:')!!}
																	{!! Form::text('empleado', $PurseIncentive->empleado, ['class' => 'form-control']) !!}
																</div>
															</div>
														</div>
													</div>
													<div class="modal-footer">
														<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
														<button class="btn btn-primary" type="submit">Guardar</button>
													</div>
												</div>
												{!! Form::close() !!}
												<!-- Termina Formulario -->
											</div>
										</div>
										@endforeach
									</tbody>
								</table>
							</div>
							@else
							<div class="alert alert-info">
								No hay registros que mostrar
							</div>
							@endif
						</div>
						<div class="tab-pane fade" id="tab2primary">
							@if ($purses->count() > 0)
							<div class="table-responsive">
								<table class="table big-table table-striped table-hover">
									<thead>
										<tr>
											<th>Cancelar</th>
											<th>NOMBRE DEL TRABAJADOR</th>
											<th>ID TGS SI VALE</th>
											<th>IDENTIFICADOR</th>
											<th>NO. TARJETA</th>
											<th>CONCEPTO</th>
											<th>REGIONES</th>
											<th>ESTATUS</th>
											<th>FECHA DE ENVIO</th>
											<th>MONTO DE FONDEO</th>
											<th>FECHA DE FONDEO</th>
											<th>NOMBRE EMPLEADO</th>
											<th>MES</th>
											<th>AÑO</th>
										</tr>
									</thead>
									<tbody>
										@foreach($purses as $PurseIncentive)  
										<tr>
											<td>
												<button
												class="btn btn-danger btn-delete-user"
												data-target="#delete-user-modal{{$PurseIncentive->id}}"
												data-toggle="modal"

												>
												<i class="fa fa-fw fa-trash"></i>
											</button>
										</td>
										<td class="trabajador-name"><span>{{ $PurseIncentive->nombre_trabajador}}</span>
										</td>
										<td><span>{{ $PurseIncentive->id_tgs_si_vale }}</span></td>
										<td><span>{{ $PurseIncentive->identificador }}</span></td>
										<td><span>{{ $PurseIncentive->no_tarjeta }}</span></td>
										<td><span>{{ $PurseIncentive->concepto }}</span></td>
										<td><span>{{ $PurseIncentive->region }}</span></td>
										<td><span>{{ $PurseIncentive->estatus }}</span></td>
										<td><span>{{ $PurseIncentive->fecha_de_envio }}</span></td>
										<td><span>$</span><span>{{ number_format($PurseIncentive->monto_de_fondeo) }}</span><span>.00</span></td>
										<td><span>{{ $PurseIncentive->fecha_de_fondeo }}</span></td>
										<td><span>{{ $PurseIncentive->empleado }}</span></td>
										<td><span>{{ $PurseIncentive->mes }}</span></td>
										<td><span>{{ $PurseIncentive->anio }}</span></td>

									</tr>
									<div class="modal fade" tabindex="-1" role="dialog" id="delete-user-modal{{$PurseIncentive->id}}">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												{!! Form::model($purses, [
												'action' => $PurseIncentive->exists ? ['assigmentsCancellations\AssigmentsIncentivosController@destroy', $PurseIncentive->id] : 'assigmentsCancellations\AssigmentsIncentivosController@destroy',$PurseIncentive->id,
												'method' => $PurseIncentive->exists ? 'DELETE' : 'DELETE',
												]) !!}
												<div class="modal-content">
													<div class="modal-body">
														¿Está seguro que desea hacer la cancelacion de <strong>{{$PurseIncentive->nombre_trabajador}}</strong>?
													</div>
													<div class="modal-footer">
														<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
														<button type="submit" class="btn btn-danger">Sí, hacer la cancelacion.</button>
													</div>
												</div>
												{!! Form::close() !!}
											</div>
											@endforeach
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</tbody>
							</table>
						</div>
						@else
						<div class="alert alert-info">
							No hay registros que mostrar
						</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('modals')
@endsection
@section('script')
<script>
	$('.btn-delete-user').click(function() {
			var action = $(this).attr('data-action');
			console.log(action);
			$('#delete-user-form').attr('action',action);
			$('#delete-user-name').text($(this).parents('tr').find('.trabajador-name').text());
		});
</script>
@endsection