@extends('app')


@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1 class="dashboard-heading">
				Asignaciones y Cancelaciones
			</h1>
			<h2 class="dashboard-heading">
				Viaticos
			</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel with-nav-tabs panel-primary">
				<div class="panel-heading">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab1primary" data-toggle="tab">Asignaciones</a></li>
						<li><a href="#tab2primary" data-toggle="tab">Cancelaciones</a></li>
					</ul>
				</div>
				<div class="panel-body" style="padding: 0px">
					<!-- Empieza Contenido tabs -->
					<div class="tab-content">
						<div class="tab-pane fade in active" id="tab1primary">
							@if ($pursesNotAssign->count() > 0)
							<div class="table-responsive">
								<table class="table big-table table-striped table-hover">
									<thead>
										<tr>
											<th>Asignar</th>
											<th>NOMBRE DEL TRABAJADOR</th>
											<th>ID TGS SI VALE</th>
											<th>IDENTIFICADOR</th>
											<th>NO. TARJETA</th>
											<th>CONCEPTO</th>
											<th>ESTATUS</th>
											<th>FECHA DE ENVIO</th>
											<th>ACUMULADO</th>
											<th>SEGMENTO</th>
											<th>NUMERO DE EMPLEADO</th>
											<th>FOLIO DE REASIGNACION</th>
										</tr>
									</thead>
									<tbody>
										@foreach($pursesNotAssign as $PurseTravelExpense)  
										<tr>
											<td>
												<button type="button" data-toggle="modal" data-target="#asignar{{ $PurseTravelExpense->id }}">
													<i class="fas fa-user-plus"></i>
												</button>
											</td>
											<td><span>{{ $PurseTravelExpense->nombre_trabajador }}</span></td>
											<td><span>{{ $PurseTravelExpense->id_tgs_si_vale }}</span></td>
											<td><span>{{ $PurseTravelExpense->identificador }}</span></td>
											<td><span>{{ $PurseTravelExpense->no_tarjeta }}</span></td>
											<td><span>{{ $PurseTravelExpense->concepto }}</span></td>
											<td><span>{{ $PurseTravelExpense->estatus }}</span></td>
											<td><span>{{ $PurseTravelExpense->fecha_de_envio }}</span></td>
											<td><span>$</span><span>{{ number_format($PurseTravelExpense->acumulado) }}</span><span>.00</span></td>
											<td><span>{{ $PurseTravelExpense->segmento }}</span></td>
											<td><span>{{ $PurseTravelExpense->empleado }}</span></td>
											<td><span>{{ $PurseTravelExpense->folio_reasignacion }}</span></td>
											
										</tr>
										<div class="modal fade" id="asignar{{ $PurseTravelExpense->id }}" tabindex="-1" role="dialog">
											<div class="modal-dialog">
												{!! Form::model($pursesNotAssign, [
													'action' => $PurseTravelExpense->exists ? ['assigmentsCancellations\AssigmentsIncentivosController@update', $PurseTravelExpense->id] : 'assigmentsCancellations\AssigmentsIncentivosController@update',$PurseTravelExpense->id,
													'method' => $PurseTravelExpense->exists ? 'PUT' : 'POST',
													]) !!}
													<div class="modal-content">
														<!-- Empieza Formulario --> 
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
															<h4 class="modal-title">Asignacion Viaticos</h4>
														</div>
														<div class="modal-body">
															<div class="row">
																<div class="col-md-12">
																	<div class="form-group">
																		{!! Form::label('nombre_trabajador', 'NOMBRE DEL TRABAJADOR:')!!}
																		{!! Form::text('nombre_trabajador', $PurseTravelExpense->nombre_trabajador, ['class' => 'form-control']) !!}
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('id_tgs_si_vale', 'ID TGS SI VALE::')!!}
																		{!! Form::text('id_tgs_si_vale', $PurseTravelExpense->id_tgs_si_vale, ['class' => 'form-control']) !!}
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('no_tarjeta', 'NO. TARJETA:')!!}
																		{!! Form::text('no_tarjeta', $PurseTravelExpense->no_tarjeta, ['class' => 'form-control']) !!}
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('identificador', 'IDENTIFICADOR:')!!}
																		{!! Form::text('identificador', $PurseTravelExpense->identificador, ['class' => 'form-control']) !!}
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('concepto', 'CONCEPTO:')!!}
																		{!! Form::text('concepto', $PurseTravelExpense->concepto, ['class' => 'form-control']) !!}
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('estatus', 'ESTATUS:')!!}
																		{!! Form::select('estatus', ['Disponible' => 'Disponible', 'Entregada' => 'Entregada', 'Cancelada' => 'Cancelada'], $PurseTravelExpense->estatus, ['class' => 'form-control']) !!}
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('fecha_de_envio', 'Fecha', ['class' => 'control-label']) !!}
																		{!! Form::date('fecha_de_envio',$PurseTravelExpense->fecha_de_envio, ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus']) !!}
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('acumulado', 'ACUMULADO:')!!}
																		{!! Form::text('acumulado', $PurseTravelExpense->acumulado, ['class' => 'form-control']) !!}
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('segmento', 'SEGMENTO:')!!}
																		{!! Form::text('segmento', $PurseTravelExpense->segmento, ['class' => 'form-control']) !!}
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('empleado', 'NUMERO DE EMPLEADO:')!!}
																		{!! Form::text('empleado', $PurseTravelExpense->empleado, ['class' => 'form-control']) !!}
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('folio_reasignacion', 'FOLIO DE REASIGNACION:')!!}
																		{!! Form::text('folio_reasignacion', $PurseTravelExpense->folio_reasignacion, ['class' => 'form-control']) !!}
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
															<button class="btn btn-primary" type="submit">Guardar</button>
														</div>
													</div>
													{!! Form::close() !!}
													<!-- Termina Formulario -->
												</div>
											</div>
											@endforeach
										</tbody>
									</table>
								</div>
								@else
								<div class="alert alert-info">
									No hay registros que mostrar
								</div>
								@endif
							</div>
							<div class="tab-pane fade" id="tab2primary">
								@if ($purses->count() > 0)
								<div class="table-responsive">
									<table class="table big-table table-striped table-hover">
										<thead>
											<tr>
												<th>Cancelar</th>
												<th>NOMBRE DEL TRABAJADOR</th>
												<th>ID TGS SI VALE</th>
												<th>IDENTIFICADOR</th>
												<th>NO. TARJETA</th>
												<th>CONCEPTO</th>
												<th>ESTATUS</th>
												<th>FECHA DE ENVIO</th>
												<th>ACUMULADO</th>
												<th>SEGMENTO</th>
												<th>NUMERO DEEMPLEADO</th>
												<th>FOLIO DE REASIGNACION</th>
											</tr>
										</thead>
										<tbody>
											@foreach($purses as $PurseTravelExpense)  
											<tr>
												<td>
													<button
													class="btn btn-danger btn-delete-user"
													data-target="#delete-user-modal{{$PurseTravelExpense->id}}"
													data-toggle="modal"

													>
													<i class="fa fa-fw fa-trash"></i>
												</button>
											</td>
											<td class="trabajador-name"><span>{{ $PurseTravelExpense->nombre_trabajador}}</span>
											</td>
											<td><span>{{ $PurseTravelExpense->id_tgs_si_vale }}</span></td>
											<td><span>{{ $PurseTravelExpense->identificador }}</span></td>
											<td><span>{{ $PurseTravelExpense->no_tarjeta }}</span></td>
											<td><span>{{ $PurseTravelExpense->concepto }}</span></td>
											<td><span>{{ $PurseTravelExpense->estatus }}</span></td>
											<td><span>{{ $PurseTravelExpense->fecha_de_envio }}</span></td>
											<td><span>$</span><span>{{ number_format($PurseTravelExpense->acumulado) }}</span><span>.00</span></td>
											<td><span>{{ $PurseTravelExpense->segmento }}</span></td>
											<td><span>{{ $PurseTravelExpense->empleado }}</span></td>
											<td><span>{{ $PurseTravelExpense->folio_reasignacion }}</span></td>
										</tr>
										<div class="modal fade" tabindex="-1" role="dialog" id="delete-user-modal{{$PurseTravelExpense->id}}">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													{!! Form::model($purses, [
														'action' => $PurseTravelExpense->exists ? ['assigmentsCancellations\AssigmentsViaticosController@destroy', $PurseTravelExpense->id] : 'assigmentsCancellations\AssigmentsViaticosController@destroy',$PurseTravelExpense->id,
														'method' => $PurseTravelExpense->exists ? 'DELETE' : 'DELETE',
														]) !!}
														<div class="modal-content">
															<div class="modal-body">
																¿Está seguro que desea hacer la cancelacion de <strong>{{$PurseTravelExpense->nombre_trabajador}}</strong>?
															</div>
															<div class="modal-footer">
																<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
																<button type="submit" class="btn btn-danger">Sí, hacer la cancelacion.</button>
															</div>
														</div>
														{!! Form::close() !!}
													</div>
													@endforeach
												</div><!-- /.modal-content -->
											</div><!-- /.modal-dialog -->
										</tbody>
									</table>
								</div>
								@else
								<div class="alert alert-info">
									No hay registros que mostrar
								</div>
								@endif

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection

	@section('modals')
	<div class="modal fade" id="delete-user-modal">
		{!! Form::open([
			'id' => 'delete-user-form',
			'method' => 'DELETE',
			'class' => 'modal-dialog modal-dm',
			]) !!}
			<div class="modal-content">
				<div class="modal-body">
					¿Está seguro que desea hacer la cancelacion?<strong id="delete-user-name"></strong>
				</div>
				<div class="modal-footer">
					<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-danger">Sí, hacer la cancelacion</button>
				</div>
			</div>
			{!! Form::close() !!}
		</div>

		@endsection
