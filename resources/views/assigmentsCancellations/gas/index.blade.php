@extends('app')


@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1 class="dashboard-heading">
				Asignaciones y Cancelaciones
			</h1>
			<h2 class="dashboard-heading">
				Gasolina
			</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel with-nav-tabs panel-primary">
				<div class="panel-heading">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab1primary" data-toggle="tab">Asignaciones</a></li>
						<li><a href="#tab2primary" data-toggle="tab">Cancelaciones</a></li>
					</ul>
				</div>
				<div class="panel-body" style="padding: 0px">
					<!-- Empieza Contenido tabs -->
					
					<div class="tab-content">
						<div class="tab-pane fade in active" id="tab1primary">
							@if ($pursesNotAssign->count() > 0)
							<div class="table-responsive">
								<table class="table big-table table-striped table-hover big-table">
									<thead>
										<tr>
											<th>Asignar</th>
											<th>NOMBRE DEL TRABAJADOR</th>
											<th>ID TGS SI VALE</th>
											<th>IDENTIFICADOR</th>
											<th>NO. TARJETA</th>
											<th>CONCEPTO</th>
											<th>ESTATUS</th>
											<th>FECHA DE ENVIO</th>
											<th>MONTO DE FONDEO</th>
											<th>NUMERO DE EMPLEADO</th>
										</tr>
									</thead>
									<tbody>
										@foreach($pursesNotAssign as $PurseGas)  
										<tr>
											<td>
												<button type="button" data-toggle="modal" data-target="#asignar{{ $PurseGas->id }}">
													<i class="fas fa-user-plus"></i>
												</button>
											</td>
											<td><span>{{ $PurseGas->nombre_trabajador}}</span></td>
											<td><span>{{ $PurseGas->id_tgs_si_vale }}</span></td>
											<td><span>{{ $PurseGas->identificador }}</span></td>
											<td><span>{{ $PurseGas->no_tarjeta }}</span></td>
											<td><span>{{ $PurseGas->concepto }}</span></td>
											<td><span>{{ $PurseGas->estatus }}</span></td>
											<td><span>{{ $PurseGas->fecha_de_envio }}</span></td>
											<td><span>$</span><span>{{ number_format($PurseGas->monto_de_fondeo)}}</span><span>.00</span></td>
											<td><span>{{ $PurseGas->empleado }}</span></td>
										</tr>
										<div class="modal fade" id="asignar{{ $PurseGas->id }}" tabindex="-1" role="dialog">
											<div class="modal-dialog">
												{!! Form::model($pursesNotAssign, [
													'action' => $PurseGas->exists ? ['assigmentsCancellations\AssigmentsGasController@update', $PurseGas->id] : 'assigmentsCancellations\AssigmentsGasController@update',$PurseGas->id,
													'method' => $PurseGas->exists ? 'PUT' : 'POST',
													]) !!}
													<div class="modal-content">
														<!-- Empieza Formulario --> 
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
															<h4 class="modal-title">Asignacion Gasolina</h4>
														</div>
														<div class="modal-body">
															<div class="row">
																<div class="col-md-12">
																	<div class="form-group">
																		{!! Form::label('nombre_trabajador', 'NOMBRE DEL TRABAJADOR:')!!}
																		{!! Form::text('nombre_trabajador', $PurseGas->nombre_trabajador, ['class' => 'form-control']) !!}
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('id_tgs_si_vale', 'ID TGS SI VALE::')!!}
																		{!! Form::text('id_tgs_si_vale', $PurseGas->id_tgs_si_vale, ['class' => 'form-control']) !!}
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('no_tarjeta', 'NO. TARJETA:')!!}
																		{!! Form::text('no_tarjeta', $PurseGas->no_tarjeta, ['class' => 'form-control']) !!}
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('identificador', 'IDENTIFICADOR:')!!}
																		{!! Form::text('identificador', $PurseGas->identificador, ['class' => 'form-control']) !!}
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('concepto', 'CONCEPTO:')!!}
																		{!! Form::text('concepto', $PurseGas->concepto, ['class' => 'form-control']) !!}
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('estatus', 'ESTATUS:')!!}
																		{!! Form::select('estatus', ['Disponible' => 'Disponible', 'Entregada' => 'Entregada', 'Cancelada' => 'Cancelada'], $PurseGas->estatus, ['class' => 'form-control']) !!}
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('fecha_de_envio', 'Fecha', ['class' => 'control-label']) !!}
																		{!! Form::date('fecha_de_envio', $PurseGas->fecha_de_envio, ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus']) !!}
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('monto_de_fondeo', 'FONDO DE FONDEO:')!!}
																		{!! Form::text('monto_de_fondeo', $PurseGas->monto_de_fondeo, ['class' => 'form-control']) !!}
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		{!! Form::label('empleado', 'NOMBRE DE EMPLEADO:')!!}
																		{!! Form::text('empleado', $PurseGas->empleado, ['class' => 'form-control']) !!}
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
															<button class="btn btn-primary" type="submit">Guardar</button>
														</div>
													</div>
													{!! Form::close() !!}
													<!-- Termina Formulario -->
												</div>
											</div>
											@endforeach
										</tbody>
									</table>
								</div>
								@else
								<div class="alert alert-info">
									No hay registros que mostrar
								</div>
								@endif
							</div>
							<div class="tab-pane fade" id="tab2primary">
								@if ($purses->count() > 0)
								<div class="table-responsive">
									<table class="table big-table table-striped table-hover">
										<thead>
											<tr>
												<th>Cancelar</th>
												<th>NOMBRE DEL TRABAJADOR</th>
												<th>ID TGS SI VALE</th>
												<th>IDENTIFICADOR</th>
												<th>NO. TARJETA</th>
												<th>CONCEPTO</th>
												<th>ESTATUS</th>
												<th>FECHA DE ENVIO</th>
												<th>MONTO DE FONDEO</th>
												<th>NO. EMPLEADO</th>

											</tr>
										</thead>
										<tbody>
											@foreach($purses as $PurseGas)  
											<tr>
												<td>
													<button
													class="btn btn-danger btn-delete-user"
													data-target="#delete-user-modal{{$PurseGas->id}}"
													data-toggle="modal"
													
													>
													<i class="fa fa-fw fa-trash"></i>
												</button>
											</td>
											<td class="trabajador-name"><span>{{ $PurseGas->nombre_trabajador}}</span>
											</td>
											<td><span>{{ $PurseGas->id_tgs_si_vale }}</span></td>
											<td><span>{{ $PurseGas->identificador }}</span></td>
											<td><span>{{ $PurseGas->no_tarjeta }}</span></td>
											<td><span>{{ $PurseGas->concepto }}</span></td>
											<td><span>{{ $PurseGas->estatus }}</span></td>
											<td><span>{{ $PurseGas->fecha_de_envio }}</span></td>
											<td><span>$</span><span>{{ number_format($PurseGas->monto_de_fondeo) }}</span><span>.00</span></td>
											<td><span>{{ $PurseGas->empleado }}</span></td>
										</tr>
										<div class="modal fade" tabindex="-1" role="dialog" id="delete-user-modal{{$PurseGas->id}}">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													{!! Form::model($purses, [
														'action' => $PurseGas->exists ? ['assigmentsCancellations\AssigmentsGasController@destroy', $PurseGas->id] : 'assigmentsCancellations\AssigmentsGasController@destroy',$PurseGas->id,
														'method' => $PurseGas->exists ? 'DELETE' : 'DELETE',
														]) !!}
														<div class="modal-content">
															<div class="modal-body">
																¿Está seguro que desea hacer la cancelacion de <strong>{{$PurseGas->nombre_trabajador}}</strong>?
															</div>
															<div class="modal-footer">
																<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
																<button type="submit" class="btn btn-danger">Sí, hacer la cancelacion.</button>
															</div>
														</div>
														{!! Form::close() !!}
													</div>
													@endforeach
												</div><!-- /.modal-content -->
											</div><!-- /.modal-dialog -->
										</div><!-- /.modal -->
									</tbody>
								</table>
							</div>
							@else
							<div class="alert alert-info">
								No hay registros que mostrar
							</div>
							@endif
						</div>
					</div>
					<!-- Termina Contenido tabs -->
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('modals')


@endsection
@section('script')
<script>
		$('.btn-delete-user').click(function() {
			var action = $(this).attr('data-action');
			console.log(action);
			$('#delete-user-form').attr('action',action);
			$('#delete-user-name').text($(this).parents('tr').find('.trabajador-name').text());
		});
</script>
@endsection
