@extends('app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h1 class="dashboard-heading">
                Disponibilidad
            </h1>
            <h2 class="dashboard-heading">
                Total Facturado
            </h2>

        </div>
        <div class="col-xs-12 col-sm-6 text-right">
            <button type="button" class="btn btn-primary btn-header registrar-factura" data-toggle="modal" data-target="#charger-manual">
                Registrar informacion <i class="fa fa-plus" aria-hidden="true"></i>
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel with-nav-tabs panel-primary">
                <div class="panel-heading">
                    <ul class="nav nav-tabs">
                        <li class="active"  id="Gasolina"><a href="#tab1primary" data-toggle="tab">Gasolina</a></li>
                        <li  id="Incentivos"><a href="#tab2primary" data-toggle="tab" id="incentivos">Incentivos</a></li>
                        <li  id="Viaticos"><a href="#tab3primary" data-toggle="tab" id="viaticos">Viaticos</a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <!-- TAB Disponibilidad Gasolina -->
                        <div class="tab-pane fade in active" id="tab1primary">
                            @if ($facturesGas->count() > 0)
                            <div class="table-responsive">
                                <table class="table table-striped big-tableCenter big-table">
                                    <thead>
                                      <tr>
                                        <th>CONCEPTO</th>
                                        <th>MONTO FONDEO</th>
                                        <th>COMISION</th>
                                        <th>TOTAL</th>
                                        <th>FECHA FONDEO2</th>
                                        <th>MES</th>
                                        <th>NUMERO DE PEDIDO</th>
                                        <th>AÑO</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    <?php $totalgas = 0; ?>
                                    @foreach($facturesGas as $TotalFacture)
                                    <tr>
                                        <td><span>{{ $TotalFacture->concepto}}</span></td>
                                        <td><span><span>$</span>{{ number_format($TotalFacture->monto_fondeo)}}</span><span>.00</span></td>
                                        <td><span><span>$</span>{{ number_format($TotalFacture->comision)}}</span><span>.00</span></td>
                                        <td><span><span>$</span>{{ number_format($TotalFacture->monto_fondeo + $TotalFacture->comision)}}<span>.00</span></span></td>
                                        <td><span>{{ $TotalFacture->fecha_fondeo}}</span></td>
                                        <td><span>{{ $TotalFacture->mes}}</span></td>
                                        <td><span>{{ $TotalFacture->numero_pedido}}</span></td>
                                        <td><span>{{ $TotalFacture->anio}}</span></td>
                                    </tr>
                                    <?php $totalgas += $TotalFacture->monto_fondeo; ?>  
                                    @endforeach
                                </tbody>
                                <thead>
                                  <tr>
                                    <th></th>
                                    <th><span>$</span>{{number_format($totalgas)}}<span>.00</span></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>  
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-7">
                            <div class="table-responsive">
                                <table class="table table-striped big-tableCenter">
                                    <thead>
                                        <tr>
                                            <th>DISPONIBLIBILIDAD</th>
                                            <th><span>$</span>{{number_format($totalgas-$totalMontoGas)}}<span>.00</span></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="alert alert-info">
                        No hay registros que mostrar
                    </div>
                    @endif
                </div>
                <!-- TAB Disponibilidad Incentivos -->
                <div class="tab-pane fade" id="tab2primary">
                    @if ($facturesIncentivos->count() > 0)
                    <div class="table-responsive">
                        <table class="table table-striped big-tableCenter big-table">
                            <thead>
                              <tr>
                                <th>CONCEPTO</th>
                                <th>MONTO FONDEO</th>
                                <th>COMISION</th>
                                <th>TOTAL</th>
                                <th>FECHA FONDEO2</th>
                                <th>MES</th>
                                <th>NUMERO DE PEDIDO</th>
                                <th>AÑO</th>
                            </tr>
                        </thead>

                        <tbody>
                          <?php $totalincentivos = 0; ?>
                          @foreach($facturesIncentivos as $TotalFacture)  
                          <tr>
                            <td><span>{{ $TotalFacture->concepto}}</span></td>
                            <td><span><span>$</span>{{ number_format($TotalFacture->monto_fondeo)}}</span><span>.00</span></td>
                            <td><span><span>$</span>{{ number_format($TotalFacture->comision)}}</span><span>.00</span></td>
                            <td><span><span>$</span>{{ number_format($TotalFacture->monto_fondeo + $TotalFacture->comision)}}<span>.00</span></span></td>
                            <td><span>{{ $TotalFacture->fecha_fondeo}}</span></td>
                            <td><span>{{ $TotalFacture->mes}}</span></td>
                            <td><span>{{ $TotalFacture->numero_pedido}}</span></td>
                            <td><span>{{ $TotalFacture->anio}}</span></td>
                        </tr>
                        <?php $totalincentivos += $TotalFacture->monto_fondeo; ?>  
                        @endforeach
                    </tbody>
                    <thead>
                      <tr>
                        <th></th>
                        <th><span>$</span>{{number_format($totalincentivos)}}<span>.00</span></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>   
            </table>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="table-responsive">
                    <table class="table table-striped big-tableCenter">
                        <thead>
                            <tr>
                                <th>DISPONIBLE</th>
                                <th><span>$</span>{{number_format($totalincentivos-$totalMontoIncentivos)}}<span>.00</span></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        @else
        <div class="alert alert-info">
            No hay registros que mostrar
        </div>
        @endif
    </div>
    <!-- TAB Disponibilidad Viaticos -->
    <div class="tab-pane fade" id="tab3primary">
        @if ($facturesViaticos->count() > 0)
        <div class="table-responsive">
            <table class="table table-striped big-tableCenter big-table">
                <thead>
                  <tr>
                    <th>CONCEPTO</th>
                    <th>MONTO FONDEO</th>
                    <th>COMISION</th>
                    <th>TOTAL</th>
                    <th>FECHA FONDEO2</th>
                    <th>MES</th>
                    <th>NUMERO DE PEDIDO</th>
                    <th>AÑO</th>
                </tr>
            </thead>

            <tbody>
             <?php $totalviaticos = 0; ?>
             @foreach($facturesViaticos as $TotalFacture)  
             <tr>
                <td><span>{{ $TotalFacture->concepto}}</span></td>
                <td><span><span>$</span>{{ number_format($TotalFacture->monto_fondeo)}}</span><span>.00</span></td>
                <td><span><span>$</span>{{ number_format($TotalFacture->comision)}}</span><span>.00</span></td>
                <td><span><span>$</span>{{  number_format($TotalFacture->monto_fondeo + $TotalFacture->comision)}}</span><span>.00</span></td>
                <td><span>{{ $TotalFacture->fecha_fondeo}}</span></td>
                <td><span>{{ $TotalFacture->mes}}</span></td>
                <td><span>{{ $TotalFacture->numero_pedido}}</span></td>
                <td><span>{{ $TotalFacture->anio}}</span></td>
            </tr>
            <?php $totalviaticos += $TotalFacture->monto_fondeo; ?>  
            @endforeach
        </tbody>
        <thead>
          <tr>
            <th></th>
            <th><span>$</span>{{number_format($totalviaticos)}}<span>.00</span></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </thead>     
</table>
</div>
<div class="row">
    <div class="col-md-7">
        <div class="table-responsive">
            <table class="table table-striped big-tableCenter">
                <thead>
                    <tr>
                        <th>DISPONIBLE</th>
                         <th><span>$</span>{{number_format($totalviaticos-$totalMontoViaticos)}}<span>.00</span></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@else
<div class="alert alert-info">
    No hay registros que mostrar
</div>
@endif
</div>
</div>
</div>
</div>
</div>
</div>
</div>

@endsection

@section('modals')
<div class="modal fade" id="charger-manual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Empieza Formulario --> 
            {!! Form::open(array('action' => 'TotalFacturesController@store','id' => 'form-facuture')) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Registrar informacion</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('concepto', 'CONCEPTO:')!!}
                            {!! Form::text('concepto', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('monto_fondeo', 'MONTO FONDEO:')!!}
                            <div class="input-group">
                                <div class="input-group-addon">$</div>
                                {!! Form::text('monto_fondeo', null, ['class' => 'form-control']) !!}

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('comision', 'COMISION:')!!}
                            <div class="input-group">
                                <div class="input-group-addon">$</div>
                                {!! Form::text('comision', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('fecha_fondeo', 'FECHA FONDEO2:')!!}
                            {!! Form::date('fecha_fondeo', Carbon\Carbon::today()->format('Y-m-d'), ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('mes', 'Mes:')!!}
                            {!! Form::select('mes', ['Enero' => 'Enero', 'Febrero' => 'Febrero','Marzo' => 'Marzo','Abril' => 'Abril','Mayo' => 'Mayo','Junio' => 'Junio','Julio' => 'Julio','Agosto' => 'Agosto','Septiembre' => 'Septiembre','Octubre' => 'Octubre','Novimebre' => 'Noviembre','Diciembre' => 'Diciembre'], 'S',['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('numero_pedido', 'NUMERO DE PEDIDO:')!!}
                            {!! Form::text('numero_pedido', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('anio', 'AÑO:')!!}
                            {!! Form::text('anio', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('purse', 'Compensación:')!!}
                            {!! Form::text('purse', null, ['class' => 'form-control','readonly' => 'readonly']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button class="btn btn-primary" type="submit">Guardar</button>
            </div>
            {!! Form::close() !!}
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
@endsection

@section('script')
<!-- <script>
    $(function() {
        $('.registrar-factura').click(function() {
            // var action = $(this).attr('data-action'); var ref_this = $('ul.tabs li a.active');
            var active = $("ul.nav li.active").attr("id");
            console.log(active);
            alert(active);
            // $('#delete-user-form').attr('action', action);
            // $('#delete-user-name').text($(this).parents('tr').find('.trabajador-name').text());
        });
    });
</script> -->
<!-- <script>
    $(document).ready(function(){
         $('.registrar-factura').click(function() {
            // var action = $(this).attr('data-action'); var ref_this = $('ul.tabs li a.active');
            var active = $("ul.nav li.active").attr("id");
            console.log(active);
            alert(active);
            // $('#delete-user-form').attr('action', action);
            // $('#delete-user-name').text($(this).parents('tr').find('.trabajador-name').text());
        });
    });
</script> -->
<script>
    $(document).ready(function() {
        $(".registrar-factura").click(function() {
            var active = $("ul.nav li.active").attr("id");
            $("#purse").val(active);

        });
    });
</script>

<script>
    $(document).ready(function() {
        $(".registrar").click(function(e) {
            e.preventDefault();
            var active = $("ul.nav li.active").attr("id");
            var form =  $('#form-facuture').attr('action');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                }
            });
            $.ajax({
                url: 'http://localhost:8000/totalFactures2',
                dataType : 'json', 
                type: 'POST',
                data: {
                    active:active, 
                },
                success: function(data) {
                    console.log("Success with data " + data);
                },
                error: function(data) {
                    console.log("Error with data " + data);
                }
            });
        });
    });


</script>
<!-- <script>
    $("#form-facuture").submit(function(e){
        e.preventDefault();
        // $("#saveEmployeeForm .form-group").removeClass("has-error");
        var active = $("ul.nav li.active").attr("id");

        $.ajax({
            beforSend:function(){

            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:"totalFactures",
            type:"POST",
            data: {id: active},
            success: function(data){
                if(!data.valid){
                    var keys = Object.keys(data.errors);
                    $.each(keys,function(index,value){
                        setErrorToField("#"+value);
                    });
                }else{
                    console.log(data);
                }
            },
            complete: function(){

            },
            error: function(err){
                console.log(err);
            }
        });
    });
</script> -->
@endsection

















