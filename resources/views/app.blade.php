  <!DOCTYPE html>
  <html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <title>Monederos</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <meta name="theme-color" content="#003776">
    <style>
    .dropdown-submenu {
      position: relative;
    }

    .dropdown-submenu .dropdown-menu {
      top: 0;
      left: 100%;
      margin-top: -1px;
    }
  </style>
  <style>
  .upload-drop-zone {
    height: 200px;
    border-width: 2px;
    margin-bottom: 20px;
  }

  /* skin.css Style*/
  .upload-drop-zone {
    color: #ccc;
    border-style: dashed;
    border-color: #ccc;
    line-height: 200px;
    text-align: center
  }
  .upload-drop-zone.drop {
    color: #222;
    border-color: #222;
  }
</style>
<style>
.with-nav-tabs.panel-primary .nav-tabs > li > a,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
  color: #fff;
}
.with-nav-tabs.panel-primary .nav-tabs > .open > a,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
  color: #fff;
  background-color: #003776;
  border-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
  color: #003776;
  background-color: #fff;
  border-color: #003776;
  border-bottom-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu {
  background-color: #003776;
  border-color: #003776;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a {
  color: #fff;   
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
  background-color: #003776;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
  background-color: #4a9fe9;
}
</style>


{!! Html::style('css/app.css') !!}

@yield('style')
</head>
<body id="app-layout" class="@if(isset($bodyClass)) {{$bodyClass}} @endif" >
  <div id="wrapper">

    <div class="overlay"></div>
    <nav class="navbar navbar-fixed-top" id="sidebar-wrapper" role="navigation">
      <ul class="nav sidebar-nav">
        <li class="sidebar-brand">
          <a href="/">
            @if(auth()->check())
            {{ Auth::user()->first_name }}
            @endif
          </a>
        </li>
        @unless(auth()->guest())
        @php
        $user = auth()->user();
        @endphp
        <li class="dropdown">
          <a href="{!!URL::to('/summaries/gas')!!}" class="dropdown-toggle" data-toggle="dropdown">GASOLINA<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{!!URL::to('/summaries/gas')!!}" style="text-align: center">Resumen Mensual</a></li>
            <li class="divider"></li>
            <li><a href="{!!URL::to('charges/gas')!!}" style="text-align: center">Carga de Información</a></li>
            <li class="divider"></li>
            <li><a href="{!!URL::to('reports/gas')!!}" style="text-align: center">Reportes</a></li>
            <li class="divider"></li>
            <li><a href="{!!URL::to('assigmentsCancellations/gas')!!}" style="text-align: center">Asignaciones y Cancelaciones</a></li>
            <li class="divider" style=""></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="{!!URL::to('/summaries/incentivos')!!}" class="dropdown-toggle" data-toggle="dropdown">INCENTIVOS<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{!!URL::to('/summaries/incentivos')!!}" style="text-align: center">Resumen Mensual</a></li>
            <li class="divider"></li>
            <li><a href="{!!URL::to('charges/incentivos')!!}" style="text-align: center">Carga de Información</a></li>
            <li class="divider"></li>
            <li><a href="{!!URL::to('reports/incentivos')!!}" style="text-align: center">Reportes</a></li>
            <li class="divider"></li>
            <li><a href="{!!URL::to('assigmentsCancellations/incentivos')!!}" style="text-align: center">Asignaciones y Cancelaciones</a></li>
            <li class="divider"></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="{!!URL::to('/summaries/viaticos')!!}" class="dropdown-toggle" data-toggle="dropdown">VIÁTICOS<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{!!URL::to('/summaries/viaticos')!!}" style="text-align: center">Resumen Mensual</a></li>
            <li class="divider"></li>
            <li><a href="{!!URL::to('charges/viaticos')!!}" style="text-align: center">Carga de Información</a></li>
            <li class="divider"></li>
            <li><a href="{!!URL::to('reports/viaticos')!!}" style="text-align: center">Reportes</a></li>
            <li class="divider"></li>
            <li><a href="{!!URL::to('assigmentsCancellations/viaticos')!!}" style="text-align: center">Asignaciones y Cancelaciones</a></li>
            <li class="divider"></li>
          </ul>
        </li>
        @if($user->role == 'admin')
        <li class="dropdown">
          <a href="" class="dropdown-toggle" data-toggle="dropdown">DISPONIBILIDAD<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{!!URL::to('/totalFactures')!!}" style="text-align: center">Total Facturado</a></li>
            <li class="divider"></li>
            <li><a href="{!!URL::to('/cardRequest')!!}" style="text-align: center">Solicitud de Tarjetas</a></li>
          </ul>
        </li>
        <li>
          <a href="/admin/users">USUARIOS</a>
        </li>
        @endif
        @endunless
      </ul>

      @unless(auth()->guest())
      <ul class="nav sidebar-nav footer">
        <li>
          <a href="/logout">Cerrar sesión</a>
        </li>
      </ul>
      @endunless
    </nav>

    <div id="page-content-wrapper">

      <div class="container-fluid app-header-wrap">
        <div class="app-header">
          @if(auth()->check())
          <a class="logo logo-header" href="/">
            <img src="/images/logo-main.png" style="padding-left: 10px;">
          </a>
          @endif          
          <div class="text-right">
            <img src="/images/logo-zurich.png" alt="">
          </div>
        </div>
      </div>
      <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
        <span class="hamb-top"></span>
        <span class="hamb-middle"></span>
        <span class="hamb-bottom"></span>
      </button>

      <div id="main-content">
        <div class="container">
          @foreach(['success', 'info', 'warning','danger'] as $item)
          @if(Session::has($item))
          <div class="alert alert-{{$item}} alert-dismissable">
            {!! Session::get($item) !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
          @endforeach
        </div>
        <div class="container">
          @yield('breadcrumbs')
        </div>
        <div class="main-container">
          @yield('content')
        </div>
      </div>

      <!--  <footer></footer> -->
    </div>

  </div>

  @yield('modals')

  {!! Html::script(asset('js/app.js')) !!}

  @yield('script')
  <script type="text/javascript">
    $.ajaxSetup({
      headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
  </script>

  <script type="text/javascript">
    $(document).ready(function () {


      $('.nav-tabs a, .js-nav-tabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
      })


      var trigger = $('.hamburger, .overlay, #sidebar-wrapper a'),
      overlay = $('.overlay'),
      isClosed = false;

      trigger.click(function () {
        hamburger_cross();
      });

      function hamburger_cross() {

        if (isClosed == true) {
          overlay.hide();
          $('.hamburger').removeClass('is-open');
          $('.hamburger').addClass('is-closed');
          isClosed = false;
        } else {
          overlay.show();
          $('.hamburger').removeClass('is-closed');
          $('.hamburger').addClass('is-open');
          isClosed = true;
        }
      }

      $('[data-toggle="offcanvas"], .overlay, #sidebar-wrapper a').click(function () {
        $('#wrapper').toggleClass('toggled');
      });

      $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
      });

      var scroll = $(document).scrollTop();
      var header = $('.app-header-wrap');
      var hamburger = $('.hamburger');
      var headerHeight = header.outerHeight();

      $(window).scroll(function() {
        var scrolled = $(document).scrollTop();
        if (scrolled > headerHeight){
          header.addClass('off-canvas');
          hamburger.addClass('filled');
        } else {
          header.removeClass('off-canvas');
          hamburger.removeClass('filled');
        }

        if (scrolled > scroll){
          header.removeClass('fixed');
        } else {
          header.addClass('fixed');
        }
        scroll = $(document).scrollTop();
      });
    });
  </script>
  <script>
    $(document).ready(function(){
      $('.dropdown-submenu a.test').on("click", function(e){
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
      });
    });
  </script>
  <script>
    $('ul.nav li.dropdown').hover(function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });
  </script>
  <script>
  /*  $(document).ready(function(){
      'use strict';

    // UPLOAD CLASS DEFINITION
    // ======================

    var dropZone = document.getElementById('drop-zone');
    var uploadForm = document.getElementById('js-upload-form');

    uploadForm.addEventListener('submit', function(e) {
      var uploadFiles = document.getElementById('js-upload-files').files;
      e.preventDefault();

      startUpload(uploadFiles)
    })


    dropZone.ondrop = function(e) {
      e.preventDefault();
      this.className = 'upload-drop-zone';

      startUpload(e.dataTransfer.files)
    }
    var startUpload = function(files) {
      console.log(files)
    }

    dropZone.ondragover = function() {
      this.className = 'upload-drop-zone drop';
      return false;
    }

    dropZone.ondragleave = function() {
      this.className = 'upload-drop-zone';
      return false;
    }

  });*/
</script>
</body>
</html>
