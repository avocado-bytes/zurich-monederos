@extends('app')

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-6">
        <h1 class="dashboard-heading">
            Resumen Mensual
        </h1>
        <h2 class="dashboard-heading">
            Gasolina
        </h2>
    </div>
</div>
<div class="table-responsive">
    <table class="table big-table table-striped table-hover big-table">
        <thead class="thead">
          <tr>
            <th>2018</th>
            <th>Enero-18</th>
            <th>Febrero-18</th>
            <th>Marzo-18</th>
            <th>Abril-18</th>
            <th>Marzo-18</th>
            <th>Junio-18</th>
            <th>Julio-18</th>
            <th>Agosto-18</th>
            <th>Septiembre-18</th>
            <th>Octubre-18</th>
            <th>Noviembre-18</th>
            <th>Diciembre-18</th>
            <th>TOTAL</th>
        </tr>
    </thead>

    <thead>
      <tr>
        <th>DISPONIBLE INICIAL</th>
        <th><span><span>$</span>{{number_format($disponibleInicial-20000)}}</span><span>.00</span></th>
        <th><span><span>$</span></span>{{number_format($disponibleInicial)}}<span>.00</span></th>
        <th><span><span>$</span></span>{{number_format($disponibleInicial)}}<span>.00</span></th>
        <th><span><span>$</span></span>{{number_format($disponibleInicial)}} <span>.00</span></th>
        <th><span><span>$</span></span>{{number_format($disponibleInicial)}} <span>.00</span></th>
        <th><span><span>$</span></span>{{number_format($disponibleInicial)}} <span>.00</span></th>
        <th><span><span>$</span></span>{{number_format($disponibleInicial)}} <span>.00</span></th>
        <th><span><span>$</span></span>{{number_format($disponibleInicial)}} <span>.00</span></th>
        <th><span><span>$</span></span>{{number_format($disponibleInicial)}} <span>.00</span></th>
        <th><span><span>$</span></span>{{number_format($disponibleInicial)}} <span>.00</span></th>
        <th><span><span>$</span></span>{{number_format($disponibleInicial)}} <span>.00</span></th>
        <th><span><span>$</span></span>{{number_format($disponibleInicial)}} <span>.00</span></th>
        <th></th>
    </tr>
</thead>
<tbody>
  <tr>
    <td>Abono Cta Concentradora</td>
    <td><span><span>$</span></span> {{number_format($abonoEnero) }}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($abonoFebrero)}}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($abonoMarzo)}}<span>.00</span> </td>
    <td><span><span>$</span></span> {{number_format($abonoAbril)}}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($abonoMayo)}}<span>.00</span> </td>
    <td><span><span>$</span></span> {{number_format($abonoJunio)}}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($abonoJulio)}}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($abonoAgosto)}}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($abonoSeptiembre)}}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($abonoOctubre)}}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($abonoNoviembre)}}<span>.00</span> </td>
    <td><span><span>$</span></span> {{number_format($abonoDiciembre)}}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($totalabono)}}<span>.00</span></td>
</tr>
<tr>
    <td>Dispersión</td>
    <td><span><span>$</span></span> {{number_format($dispersionEnero) }}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($dispersionFebrero)}}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($dispersionMarzo)}}<span>.00</span> </td>
    <td><span><span>$</span></span> {{number_format($dispersionAbril)}}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($dispersionMayo)}}<span>.00</span> </td>
    <td><span><span>$</span></span> {{number_format($dispersionJunio)}}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($dispersionJulio)}}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($dispersionAgosto)}}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($dispersionSeptiembre)}}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($dispersionOctubre)}}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($dispersionNoviembre)}}<span>.00</span> </td>
    <td><span><span>$</span></span> {{number_format($dispersionDiciembre)}}<span>.00</span></td>
    <td><span><span>$</span></span> {{number_format($totaldispersion)}}<span>.00</span></td>
</tr>
</tbody>
<thead>
  <tr>
    <th>DISPONIBLE FINAL</th>
    <th><span><span>$</span></span> {{number_format($disponibleFinalEnero) }}<span>.00</span></th>
    <th><span><span>$</span></span> {{number_format($disponibleFinalFebrero)}}<span>.00</span></th>
    <th><span><span>$</span></span> {{number_format($disponibleFinalMarzo)}}<span>.00</span> </th>
    <th><span><span>$</span></span> {{number_format($disponibleFinalAbril)}}<span>.00</span></th>
    <th><span><span>$</span></span> {{number_format($disponibleFinalMayo)}}<span>.00</span> </th>
    <th><span><span>$</span></span> {{number_format($disponibleFinalJunio)}}<span>.00</span></th>
    <th><span><span>$</span></span> {{number_format($disponibleFinalJulio)}}<span>.00</span></th>
    <th><span><span>$</span></span> {{number_format($disponibleFinalAgosto)}}<span>.00</span></th>
    <th><span><span>$</span></span> {{number_format($disponibleFinalSeptiembre)}}<span>.00</span></th>
    <th><span><span>$</span></span> {{number_format($disponibleFinalOctubre)}}<span>.00</span></th>
    <th><span><span>$</span></span> {{number_format($disponibleFinalNoviembre)}}<span>.00</span> </th>
    <th><span><span>$</span></span> {{number_format($disponibleFinalDiciembre)}}<span>.00</span></th>
    <th></th>
</tr>
</thead>
</table>
</div>
</div>

@endsection
