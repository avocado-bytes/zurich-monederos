@extends('app')


@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-6">
			<h1 class="dashboard-heading">
				Asignaciones y Cancelaciones
			</h1>
			<h2 class="dashboard-heading">
				Gasolina
			</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="panel with-nav-tabs panel-primary">
				<div class="panel-heading">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab1primary" data-toggle="tab">Asignaciones</a></li>
						<li><a href="#tab2primary" data-toggle="tab">Cancelaciones</a></li>
					</ul>
				</div>
				<div class="panel-body">
					<div class="tab-content">
						<div class="tab-pane fade in active" id="tab1primary">
							<div class="row" style="padding-bottom: 20px">
								<div class="col-sm-3 col-sm-offset-3"  style="background-color: #CCCCCC; border-radius: 6px">
									No. de Tarjeta  <i class="fas fa-times"></i>
								</div>
							</div>
							<div class="row" style="padding-bottom: 20px">
								<div class="col-md-3 col-md-offset-3" style="background-color: #CCCCCC; border-radius: 6px">
									Asignación <i class="fas fa-times"></i>
								</div>
							</div>
							<div class="row" style="padding-bottom: 20px">
								<div class="col-md-3 col-md-offset-3"  style="background-color: #CCCCCC; border-radius: 6px">
									Tipo <i class="fas fa-times"></i>
								</div>
							</div>
							<div class="row" style="padding-bottom: 20px">
								<div class="col-md-3 col-md-offset-3"  style="background-color: #CCCCCC; border-radius: 6px">
									Identificador <i class="fas fa-times"></i>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="tab2primary">
							<div class="row" style="padding-bottom: 20px">
								<div class="col-md-3 col-md-offset-3"  style="background-color: #CCCCCC; border-radius: 6px">
									No. de Tarjeta <i class="fas fa-times"></i>
								</div>
							</div>
							<div class="row" style="padding-bottom: 20px">
								<div class="col-md-3 col-md-offset-3"  style="background-color: #CCCCCC; border-radius: 6px">
									Asignación <i class="fas fa-times"></i>
								</div>
							</div>
							<div class="row" style="padding-bottom: 20px">
								<div class="col-md-3 col-md-offset-3"  style="background-color: #CCCCCC; border-radius: 6px">
									Tipo <i class="fas fa-times"></i>
								</div>
							</div>
							<div class="row" style="padding-bottom: 20px">
								<div class="col-md-3 col-md-offset-3"  style="background-color: #CCCCCC; border-radius: 6px">
									Identificador <i class="fas fa-times"></i>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="tab3primary">Primary 3</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 text-center">
			<button type="button" class="btn btn-primary btn-header" data-toggle="modal" data-target="#charger-manual">
				Guardar
			</button>
		</div>
		<div class="col-sm-3 text-center">
			<button type="button" class="btn btn-danger btn-header" data-toggle="modal" data-target="#charger-excel">
				Cancelar
			</button>
		</div>
	</div>
</div>

@endsection

