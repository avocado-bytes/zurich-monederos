<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

Route::group([
    'middleware' => 'auth',
], function() {

    Route::get('/','Summaries\GasController@index');

    Route::group([
        'prefix' => 'admin',
        'namespace' => 'Admin',
    ], function() {
        Route::resource('users', 'UsersController');
    });


    Route::group([
        'prefix' => 'summaries',
        'namespace' => 'Summaries',
    ], function() {
        Route::resource('gas', 'GasController');
        Route::resource('incentivos', 'IncentivosController');
        Route::resource('viaticos', 'ViaticosController');
    });
    Route::group([
        'prefix' => 'reports',
        'namespace' => 'reports',
    ], function() {
        Route::resource('gas', 'GasReportsController');
        Route::resource('incentivos', 'IncentivosReportsController');
        Route::resource('viaticos', 'ViaticosReportsController');
        Route::get('downloadExcel/{type}', 'GasReportsController@downloadExcel');
        Route::resource('excel','GasReportsController@export');


    });

    Route::group([
        'prefix' => 'assigmentsCancellations',
        'namespace' => 'assigmentsCancellations',
    ], function() {
        Route::resource('gas', 'AssigmentsGasController');
        Route::resource('incentivos', 'AssigmentsIncentivosController');
        Route::resource('viaticos', 'AssigmentsViaticosController');
    });

    Route::group([
        'prefix' => 'charges',
        'namespace' => 'Charges',
    ], function() {
        Route::resource('gas', 'GasChargesController');
        Route::resource('incentivos', 'IncentivosChargesController');
        Route::resource('viaticos', 'ViaticosChargesController');
        
         Route::post('ImportPurseGas', 'GasChargesController@ImportPurses');
         Route::post('ImportPurseIncentive', 'IncentivosChargesController@ImportPurses');
         Route::post('ImportPurseTravel', 'ViaticosChargesController@ImportPurses');
    });

    Route::get('totalFactures','TotalFacturesController@index');
    Route::get('cardRequest','CardRequestController@index');
    Route::post('cardRequest','CardRequestController@store');
    Route::post('totalFactures2','TotalFacturesController@store');
    
    Route::post('excelgas','reports\GasReportsController@export');
    Route::post('excelincentivos','reports\IncentivosReportsController@export');
    Route::post('excelviaticos','reports\ViaticosReportsController@export');
 

});


Route::get('/preview-report', function () {
    return new App\Mail\ReportGenerated;
});

