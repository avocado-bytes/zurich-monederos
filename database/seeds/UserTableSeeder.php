<?php

use Illuminate\Database\Seeder;
use ZurichMonederos\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::unguard();

        User::create([
            'email' => 'masalazarme@zurichsantander.com.mx',
            'first_name' => 'Miguel Ángel',
            'last_name' => 'Salazar Mendez',
            'password' => bcrypt('Tesoreria@1'),
            'role' => 'tesoreria',
        ]);

        User::create([
            'email' => 'cobranza@zurichsantander.com.mx',
            'first_name' => 'Cobranza',
            'last_name' => '',
            'password' => bcrypt('password'),
            'role' => 'cobranza',
        ]);

        User::create([
            'email' => 'riesgos@zurichsantander.com.mx',
            'first_name' => 'Riesgos',
            'last_name' => '',
            'password' => bcrypt('password'),
            'role' => 'riesgos',
        ]);

        User::create([
            'email' => 'admin@zurichsantander.com.mx',
            'first_name' => 'Miguel Ángel',
            'last_name' => 'Salazar Mendez',
            'password' => bcrypt('Admin@1'),
            'role' => 'admin',
        ]);

        User::reguard();
    }
}
