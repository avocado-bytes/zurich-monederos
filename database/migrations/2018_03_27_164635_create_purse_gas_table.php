<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurseGasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purse_gas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_trabajador')->nullable();
            $table->integer('id_tgs_si_vale');
            $table->string('identificador');
            $table->string('no_tarjeta');
            $table->string('concepto');
            $table->enum('estatus', ['Entregada', 'Disponible','Cancelada']); 
            $table->date('fecha_de_envio')->nullable();
            $table->decimal('monto_de_fondeo', 10, 2)->nullable();
            $table->string('empleado')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purse_gas');
    }
}
