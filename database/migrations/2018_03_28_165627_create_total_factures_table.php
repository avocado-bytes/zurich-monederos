<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTotalFacturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('total_factures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('concepto');
            $table->decimal('monto_fondeo',10,2);
            $table->decimal('comision',10,2);
            $table->decimal('total',10,2);
            $table->date('fecha_fondeo');
            $table->string('mes');
            $table->integer('numero_pedido')->nullable();
            $table->integer('anio');
            $table->enum('purse', ['gas','travel_expensive','incentives']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('total_factures');
    }
}
