<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurseIncentivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purse_incentives', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_trabajador')->nullable();
            $table->integer('id_tgs_si_vale');
            $table->string('identificador');
            $table->string('no_tarjeta');
            $table->string('concepto');
            $table->enum('region', ['Corporativo Call Center', 'Corporativo Asesores','Corporativo','Metro Sur', 'Metro Norte','Norte', 'Noreste','Noroeste', 'Centro','Sur', 'Sureste','Occidente','Sin Región'])->nullable(); 
            $table->enum('estatus', ['Disponible','Cancelado','Enviado']); 
            $table->date('fecha_de_envio')->nullable();
            $table->decimal('monto_de_fondeo', 10, 2)->nullable();
            $table->date('fecha_de_fondeo')->nullable();
            $table->string('empleado')->nullable();
            $table->enum('mes', ['Enero', 'Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'])->nullable();
            $table->integer('anio')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purse_incentives');
    }
}
