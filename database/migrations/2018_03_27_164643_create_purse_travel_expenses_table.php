<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurseTravelExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purse_travel_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_trabajador')->nullable();
            $table->integer('id_tgs_si_vale');
            $table->string('identificador');
            $table->string('no_tarjeta');
            $table->string('concepto');
            $table->enum('estatus', ['Entregada', 'Disponible','Cancelada']); 
            $table->date('fecha_de_envio')->nullable();
            $table->decimal('acumulado', 10, 2)->nullable();
            $table->string('segmento')->nullable();
            $table->string('empleado')->nullable();
            $table->bigInteger('folio_reasignacion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purse_travel_expenses');
    }
}
